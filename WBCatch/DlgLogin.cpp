// DlgLogin.cpp : implementation file
//

#include "stdafx.h"
#include "WBCatch.h"
#include "DlgLogin.h"
#include "SQLiteHelper.h"
#include "common.h"
#include "CheckKey.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDlgLogin dialog
#define USER_NAME_MAX_TEXT             20
#define PASSWODR_MAX_TEXT              20
#define SERVER_MAX_TEXT                20
#define PORT_MAX_TEXT                  6

CDlgLogin::CDlgLogin(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgLogin::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgLogin)
	m_strPwd = _T("");
	m_strServer = _T("");
	m_strUserName = _T("");
	m_nPort = 0;
	m_bRememberPwd = FALSE;
	m_nCamServerID = 0;
	//}}AFX_DATA_INIT
}


void CDlgLogin::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgLogin)
	DDX_Control(pDX, IDC_CHE_REMEMBERPWD, m_chRememberPwd);
	DDX_Control(pDX, IDC_EDT_USERNAME, m_edtUserName);
	DDX_Control(pDX, IDC_EDT_SERVER, m_edtServer);
	DDX_Control(pDX, IDC_EDT_PWD, m_edtPassword);
	DDX_Control(pDX, IDC_EDT_PORT, m_edtPort);
	DDX_Text(pDX, IDC_EDT_PWD, m_strPwd);
	DDX_Text(pDX, IDC_EDT_SERVER, m_strServer);
	DDX_Text(pDX, IDC_EDT_USERNAME, m_strUserName);
	DDX_Text(pDX, IDC_EDT_PORT, m_nPort);
	DDX_Check(pDX, IDC_CHE_REMEMBERPWD, m_bRememberPwd);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgLogin, CDialog)
	//{{AFX_MSG_MAP(CDlgLogin)
	ON_BN_CLICKED(IDC_BTN_LOGIN, OnBtnLogin)
	ON_BN_CLICKED(IDC_BTN_CANCEL, OnBtnCancel)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDlgLogin message handlers
BOOL CDlgLogin::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_edtUserName.SetLimitText(USER_NAME_MAX_TEXT);
	m_edtPassword.SetLimitText(PASSWODR_MAX_TEXT);
	m_edtServer.SetLimitText(SERVER_MAX_TEXT);
	m_edtPort.SetLimitText(PORT_MAX_TEXT);
	
	CSQLiteHelper *	pSqlHelper = CSQLiteHelper::GetObject();
	pSqlHelper->ExecuteSQL(_T("select username,pwd,servername,port,remember from tb_userinfo;"), SQLiteUserInfo, this);
	SuperDog_CheckKey();
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CDlgLogin::OnBtnLogin() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);

	do 
	{
		if (m_strUserName.IsEmpty())
		{
			AfxMessageBox(_T("�������û�����"));
			break;
		}
		if (m_strPwd.IsEmpty())
		{
			AfxMessageBox(_T("���������룡"));
			break;
		}
		if (m_strServer.IsEmpty())
		{
			AfxMessageBox(_T("�������������ַ��"));
			break;
		}
		if (m_nPort <= 0)
		{
			AfxMessageBox(_T("������˿ںţ�"));
			break;
		}
		CCameraMngr* pCameraManager = CCameraMngr::getInstance();
		CAM_SERVER_INFO serverInfo;
#ifdef  UNICODE
		char	szServer[SERVER_MAX_TEXT + 1] = {0};
		char	szUserName[USER_NAME_MAX_TEXT + 1] = {0};
		char	szPassword[PASSWODR_MAX_TEXT + 1] = {0};

		WideCharToMultiByte(CP_ACP, 0, m_strServer, -1, szServer, SERVER_MAX_TEXT, NULL, NULL);
		WideCharToMultiByte(CP_ACP, 0, m_strUserName, -1, szUserName, USER_NAME_MAX_TEXT, NULL, NULL);
		WideCharToMultiByte(CP_ACP, 0, m_strPwd, -1, szPassword, PASSWODR_MAX_TEXT, NULL, NULL);
		if (pCameraManager->ConnectServer(szServer, m_nPort, szUserName, szPassword, &serverInfo) != MP_ENG_OK)
#else
		if (pCameraManager->ConnectServer(m_strServer, m_nPort, m_strUserName, m_strPwd, &serverInfo) != MP_ENG_OK)
#endif
		{
			AfxMessageBox(_T("��¼ʧ�ܣ�"));
			break;
		}
		m_nCamServerID = serverInfo.nCamServerID;

		CSQLiteHelper *	pSqlHelper = CSQLiteHelper::GetObject();
		CString	strSQL;
		if (m_bRememberPwd)
		{
			strSQL.Format(_T("replace into tb_userinfo (id, username, pwd, servername, port, remember) values(%d, '%s', '%s', '%s', %d, %d);"), \
				0, m_strUserName.GetBuffer(0), m_strPwd.GetBuffer(0), m_strServer.GetBuffer(0), m_nPort, m_bRememberPwd);
		}
		else
		{
			strSQL.Format(_T("replace into tb_userinfo (id, username, pwd, servername, port, remember) values(%d, '%s', '%s', '%s', %d, %d);"), \
				0, m_strUserName.GetBuffer(0), _T(""), m_strServer.GetBuffer(0), m_nPort, m_bRememberPwd);
		}
		std::string szUTF8 = FormatUTF8(strSQL);
		pSqlHelper->ExecuteSQL(szUTF8.c_str(), NULL, NULL);

		OnOK();
	} while (0);
}

void CDlgLogin::OnBtnCancel() 
{
	// TODO: Add your control notification handler code here
	OnCancel();
}

int CDlgLogin::SQLiteUserInfo(void* pParam, int nCount, char** pValue, char** pName)
{
	CDlgLogin * pThis = static_cast<CDlgLogin *>(pParam);
	std::map<std::string, std::string>	UserInfo;
	for (int i = 0; i < nCount; i++)
	{
		UserInfo.insert( std::make_pair(pName[i], pValue[i]) );
	}
	std::map<std::string, std::string>::iterator itFind = UserInfo.find("username");
	if (itFind != UserInfo.end())
	{
#ifndef UNICODE
		pThis->SetUserName(itFind->second.c_str());
#endif
	}

	itFind = UserInfo.find("pwd");
	if (itFind != UserInfo.end())
	{
#ifndef UNICODE
		pThis->SetPassword(itFind->second.c_str());
#endif
	}

	itFind = UserInfo.find("servername");
	if (itFind != UserInfo.end())
	{
#ifndef UNICODE
		pThis->SetServerName(itFind->second.c_str());
#endif
	}

	itFind = UserInfo.find("port");
	if (itFind != UserInfo.end())
	{
#ifndef UNICODE
		pThis->SetPort(atoi(itFind->second.c_str()));
#endif
	}
	
	itFind = UserInfo.find("remember");
	if (itFind != UserInfo.end())
	{
#ifndef UNICODE
		pThis->SetRememberPwd(atoi(itFind->second.c_str()));
#endif
	}
	return 0;
}

void CDlgLogin::SetUserName(LPCTSTR szUserName)
{
	m_strUserName = szUserName;
	UpdateData(FALSE);
}

void CDlgLogin::SetPassword(LPCTSTR szPwd)
{
	m_strPwd = szPwd;
	UpdateData(FALSE);
}

void CDlgLogin::SetServerName(LPCTSTR szServerName)
{
	m_strServer = szServerName;
	UpdateData(FALSE);
}

void CDlgLogin::SetPort(int nPort)
{
	m_nPort = nPort;
	UpdateData(FALSE);
}

void CDlgLogin::SetRememberPwd(BOOL bRemember)
{
	m_bRememberPwd = bRemember;
	UpdateData(FALSE);
}

int CDlgLogin::GetCamServerID()
{
	return m_nCamServerID;
}
