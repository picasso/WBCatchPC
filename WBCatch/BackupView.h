#if !defined(AFX_PLAYBACKUP_H__9F2422AB_F494_451D_8B0B_69B59550EFAB__INCLUDED_)
#define AFX_PLAYBACKUP_H__9F2422AB_F494_451D_8B0B_69B59550EFAB__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PlayBackup.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CBackupView window

class CBackupView : public CWnd
{
// Construction
public:
	CBackupView();

// Attributes
public:

// Operations
	BOOL   CreateWnd( DWORD dwExStyle, DWORD dwStyle, const CRect& rc, CWnd* pParent, UINT uID );
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPlayBackup)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CBackupView();

	// Generated message map functions
protected:
	//{{AFX_MSG(CBackupView)
	afx_msg void OnPaint();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	void SetBackupPos(int nPos);
	int GetBackupPos();
	BOOL IsCapture();
	void SetCaptureStatus(BOOL bIsCap);
	void SetBackupSpeed(int nSpeed);
	int GetBackupSpeed();
	BOOL IsRunning();
	void Quit();
	BOOL DrawFrameImage();
	void SetPlayer(int nPlayer);
	void SetCardsNum(int nCardsNum);
	static DWORD WINAPI PlayProc(LPVOID pParameter);
private:
	int		m_nSpeed;
	HANDLE	m_hThread;
	BOOL	m_bRunning;
	BOOL	m_bIsCapture;
	int		m_nPlayer;
	int		m_nCardsNum;
	int		m_nBackupPos;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PLAYBACKUP_H__9F2422AB_F494_451D_8B0B_69B59550EFAB__INCLUDED_)
