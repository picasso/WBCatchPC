// PostMessage.cpp: implementation of the CPostMessage class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "wbcatch.h"
#include "PostMessage.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CPostMessage::CPostMessage()
{
	//::InitializeCriticalSection(&m_cs);
}

CPostMessage::~CPostMessage()
{
	//::DeleteCriticalSection(&m_cs);
}

CPostMessage * CPostMessage::GetObject()
{
	static CPostMessage	object;
	return &object;
}

BOOL CPostMessage::RegisterMessage(HWND hWnd, UINT uMsg)
{
	BOOL	bRet = FALSE;
	
	//::EnterCriticalSection(&m_cs);
	if (IsWindow(hWnd))
	{
		m_mapMsg.insert(std::make_pair(uMsg, hWnd));
		bRet = TRUE;
	}
	//::LeaveCriticalSection(&m_cs);

	return	bRet;
}

BOOL CPostMessage::UnRegisterMessage(UINT uMsg)
{
	BOOL	bRet = FALSE;
	
	//::EnterCriticalSection(&m_cs);
	std::map<UINT,HWND>::iterator it = m_mapMsg.find(uMsg);
	if (it != m_mapMsg.end())
	{
		m_mapMsg.erase(uMsg);
		bRet = TRUE;
	}
	//::LeaveCriticalSection(&m_cs);
	
	return	bRet;
}

void CPostMessage::PostMessage(UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	//::EnterCriticalSection(&m_cs);
	std::map<UINT,HWND>::iterator it = m_mapMsg.find(uMsg);
	if (it != m_mapMsg.end())
	{
		::PostMessage(it->second, uMsg, wParam, lParam);
	}
	//::LeaveCriticalSection(&m_cs);
}