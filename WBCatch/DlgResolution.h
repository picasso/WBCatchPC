#if !defined(AFX_DLGRESOLUTION_H__BA8C4E2F_6466_46E1_9BB5_E78BA5141F72__INCLUDED_)
#define AFX_DLGRESOLUTION_H__BA8C4E2F_6466_46E1_9BB5_E78BA5141F72__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgResolution.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDlgResolution dialog

class CDlgResolution : public CDialog
{
// Construction
public:
	void SetResolution(int nResolution);
	CDlgResolution(int nCamServerID, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDlgResolution)
	enum { IDD = IDD_DLG_RESOLUTION };
	CComboBox	m_CombResolution;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDlgResolution)
	protected:
	virtual BOOL OnInitDialog();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDlgResolution)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	int m_nCamServerID;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGRESOLUTION_H__BA8C4E2F_6466_46E1_9BB5_E78BA5141F72__INCLUDED_)
