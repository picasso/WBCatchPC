// DlgResolution.cpp : implementation file
//

#include "stdafx.h"
#include "wbcatch.h"
#include "DlgResolution.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDlgResolution dialog


CDlgResolution::CDlgResolution(int nCamServerID, CWnd* pParent /*=NULL*/)
	: CDialog(CDlgResolution::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgResolution)
	m_nCamServerID = nCamServerID;
	//}}AFX_DATA_INIT
}


void CDlgResolution::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgResolution)
	DDX_Control(pDX, IDC_COMB_RESOLUTION, m_CombResolution);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgResolution, CDialog)
	//{{AFX_MSG_MAP(CDlgResolution)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDlgResolution message handlers

BOOL CDlgResolution::OnInitDialog()
{
	CDialog::OnInitDialog();
	
	m_CombResolution.AddString(_T("��"));
	m_CombResolution.AddString(_T("��"));
	m_CombResolution.AddString(_T("С"));
	m_CombResolution.SetCurSel(0);
	return TRUE;
}

void CDlgResolution::SetResolution(int nResolution)
{
}
