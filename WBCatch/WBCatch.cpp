// WBCatch.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "WBCatch.h"
#include "WBCatchDlg.h"
#include "DlgLogin.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CWBCatchApp

BEGIN_MESSAGE_MAP(CWBCatchApp, CWinApp)
	//{{AFX_MSG_MAP(CWBCatchApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CWBCatchApp construction

CWBCatchApp::CWBCatchApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CWBCatchApp object

CWBCatchApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CWBCatchApp initialization

BOOL CWBCatchApp::InitInstance()
{
	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif
	//װ��gdi+
	Gdiplus::GdiplusStartup(&m_pGdiToken,&m_gdiplusStartupInput,NULL);
	do 
	{
		CPlaybackEngine::Initialize("{D79399DA-2F36-4f7d-846A-292C90BA9E8D}");

		//��¼
		CDlgLogin dlgLogin;
		int nResponse = dlgLogin.DoModal();
		if (nResponse != IDOK) break;
		CWBCatchDlg dlg;
		m_pMainWnd = &dlg;
		dlg.SetCamServerID(dlgLogin.GetCamServerID());
		nResponse = dlg.DoModal();
	} while (0);

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}

int CWBCatchApp::ExitInstance() 
{
	// TODO: Add your specialized code here and/or call the base class
	//ж��gdi+
	Gdiplus::GdiplusShutdown(m_pGdiToken);
	return CWinApp::ExitInstance();
}
