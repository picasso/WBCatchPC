#if !defined(AFX_DLGLOGIN_H__9050DEB2_885C_4885_9F47_A5850443CF0A__INCLUDED_)
#define AFX_DLGLOGIN_H__9050DEB2_885C_4885_9F47_A5850443CF0A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgLogin.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDlgLogin dialog

class CDlgLogin : public CDialog
{
// Construction
public:
	CDlgLogin(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDlgLogin)
	enum { IDD = IDD_DLG_LOGIN };
	CButton	m_chRememberPwd;
	CEdit	m_edtUserName;
	CEdit	m_edtServer;
	CEdit	m_edtPassword;
	CEdit	m_edtPort;
	CString	m_strPwd;
	CString	m_strServer;
	CString	m_strUserName;
	int		m_nPort;
	BOOL	m_bRememberPwd;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDlgLogin)
	protected:
	virtual BOOL OnInitDialog();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDlgLogin)
	afx_msg void OnBtnLogin();
	afx_msg void OnBtnCancel();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	int GetCamServerID();
	static int SQLiteUserInfo(void* pParam, int nCount, char** pValue, char** pName);
	void SetUserName(LPCTSTR szUserName);
	void SetPassword(LPCTSTR szPwd);
	void SetServerName(LPCTSTR szServerName);
	void SetPort(int nPort);
	void SetRememberPwd(BOOL bRemember);
private:
	int m_nCamServerID;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGLOGIN_H__9050DEB2_885C_4885_9F47_A5850443CF0A__INCLUDED_)
