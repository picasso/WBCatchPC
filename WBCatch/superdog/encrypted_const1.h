//    Source data: 6334501639174057234

#define ENCRYPT_CONST_FEATUREID1 1    //feature id which is selected
#define ENCRYPT_CONST_BUFSIZE1 16    //Size of encrypt constants
#define PRINT_CONST_VALUE1(p) {printf("The decrypted value is: %I64u.\n", p);}

typedef UINT64 ENCRYPT_DATA_TYPE1;
ENCRYPT_DATA_TYPE1 g_constValue1;

unsigned char encryptConstArr1[16] = { 
   0x35, 0x46, 0xE5, 0xEF, 0x35, 0x56, 0x38, 0x51, 0x98, 0xC2, 0xF5, 0x9A, 0x40, 0xB4, 0xDF, 0xDD
 };
