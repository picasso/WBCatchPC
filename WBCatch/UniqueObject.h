// UniqueObject.h: interface for the CUniqueObject class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_UNIQUEOBJECT_H__C0B82A17_03AB_44AA_B804_DC7CD25C7C6A__INCLUDED_)
#define AFX_UNIQUEOBJECT_H__C0B82A17_03AB_44AA_B804_DC7CD25C7C6A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

typedef struct
{
	BYTE *  pRGB;//RGB数据
	DWORD	dwSize;//缓冲区大小
	int     nWidth;//图片宽带
	int     nHeight;//图片高度
}RGBIMAGEINFO, * PRGBIMAGEINFO;

class CUniqueObject  
{
protected:
	CUniqueObject();
public:
	virtual ~CUniqueObject();
public:
	static CUniqueObject * GetObject();
	void Lock();
	void UnLock();
	std::map<int,HBITMAP> * GetCaptureImageMap();
	void ClearCaptureImageMap();
	int InsertCaptureImage(HBITMAP hBitmap);
	int GetCaptureImageCount();
	HBITMAP FindCaptureImage(int nIndex);
	std::map<int,HBITMAP> * GetSelectedList();
	void ClearSelected();
public:
	void InsertMemory(LPARAM lParam, RGBIMAGEINFO info);
	RGBIMAGEINFO FindMemory(LPARAM lParam);
	void DeleteMemory(LPARAM lParam);
	void ClearMemory();
private:
	CRITICAL_SECTION						m_cs;
	std::map<int,HBITMAP>					m_mapCaptureImage;
	std::map<int,HBITMAP>					m_listSelected;
	std::map<LPARAM, RGBIMAGEINFO>			m_mapMemory;
};

#endif // !defined(AFX_UNIQUEOBJECT_H__C0B82A17_03AB_44AA_B804_DC7CD25C7C6A__INCLUDED_)
