#if !defined(AFX_DLGBROWSE_H__C2DCD38D_2543_4559_964E_3921339659C1__INCLUDED_)
#define AFX_DLGBROWSE_H__C2DCD38D_2543_4559_964E_3921339659C1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgBrowse.h : header file
//
#include "ListCtrlWnd.h"
/////////////////////////////////////////////////////////////////////////////
// CDlgBrowse dialog

class CDlgBrowse : public CDialog
{
// Construction
public:
	CDlgBrowse(int nPlayers, int nCardsNum, int nOrder, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDlgBrowse)
	enum { IDD = IDD_DLG_BROWSE };
	CButton	m_btnDown;
	CButton	m_btnUp;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDlgBrowse)
	protected:
	virtual BOOL OnInitDialog();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDlgBrowse)
	afx_msg void OnClose();
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnBtnUp();
	afx_msg void OnBtnDown();
	afx_msg LRESULT OnUpDown(WPARAM wParam, LPARAM lParam);
	afx_msg void OnGetMinMaxInfo( MINMAXINFO FAR* lpMMI );
	afx_msg BOOL OnEraseBkgnd( CDC* pDC );
	afx_msg void OnPaint();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	int m_nPlayers;
	int m_nCardsNum;
	int	m_nOrder;
	CListCtrlWnd * m_pList;
	std::list<CStatic *>	m_TipList;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGBROWSE_H__C2DCD38D_2543_4559_964E_3921339659C1__INCLUDED_)
