; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CDlgBrowse
LastTemplate=generic CWnd
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "wbcatch.h"
LastPage=0

ClassCount=8
Class1=CDlgLogin
Class2=CWBCatchApp
Class3=CWBCatchDlg

ResourceCount=5
Resource1=IDD_DLG_PROPERTY
Resource2=IDD_DLG_LOGIN
Class4=CDlgProrertySet
Resource3=IDD_WBCATCH_DIALOG
Class5=CDlgResolution
Resource4=IDD_DLG_RESOLUTION
Class6=CDlgBrowse
Class7=CPlayBackup
Class8=CPlayView
Resource5=IDD_DLG_BROWSE

[CLS:CDlgLogin]
Type=0
BaseClass=CDialog
HeaderFile=DlgLogin.h
ImplementationFile=DlgLogin.cpp

[CLS:CWBCatchApp]
Type=0
BaseClass=CWinApp
HeaderFile=WBCatch.h
ImplementationFile=WBCatch.cpp
Filter=N
VirtualFilter=AC

[CLS:CWBCatchDlg]
Type=0
BaseClass=CDialog
HeaderFile=WBCatchDlg.h
ImplementationFile=WBCatchDlg.cpp
LastObject=IDC_COMB_ORDER
Filter=D
VirtualFilter=dWC

[DLG:IDD_DLG_LOGIN]
Type=1
Class=CDlgLogin
ControlCount=13
Control1=IDC_STATIC,static,1342308352
Control2=IDC_STATIC,static,1342308352
Control3=IDC_STATIC,static,1342308352
Control4=IDC_STATIC,static,1342308352
Control5=IDC_EDT_USERNAME,edit,1350631552
Control6=IDC_EDT_PWD,edit,1350631584
Control7=IDC_EDT_SERVER,edit,1350631552
Control8=IDC_EDT_PORT,edit,1350631552
Control9=IDC_BTN_LOGIN,button,1342242817
Control10=IDC_BTN_CANCEL,button,1342242816
Control11=IDC_CHE_REMEMBERPWD,button,1342242819
Control12=IDC_STATIC,button,1342177287
Control13=IDC_STATIC,button,1342177287

[DLG:IDD_WBCATCH_DIALOG]
Type=1
Class=CWBCatchDlg
ControlCount=23
Control1=IDC_STATIC_VIDEO,static,1342308352
Control2=IDC_STATIC_BACKPLAY,static,1342308352
Control3=IDC_COMB_DEVICE_LIST,combobox,1344339971
Control4=IDC_BTN_START_CAPTURE,button,1342242816
Control5=IDC_BTN_STOP_CAPTURE,button,1476460544
Control6=IDC_BTN_BROWSE,button,1342242816
Control7=IDC_BTN_PROPERTY,button,1476460544
Control8=IDC_BTN_RESOLUTION,button,1476460544
Control9=IDC_STATIC_PLAYERS,static,1342308352
Control10=IDC_COMB_PLAYERS,combobox,1344339971
Control11=IDC_STATIC_MAX_CAPTURE,static,1342308352
Control12=IDC_EDT_MAX_CAPTURE,edit,1350641792
Control13=IDC_SPIN_MAX_CAPTURE,msctls_updown32,1342177334
Control14=IDC_STATIC_CAMERA,static,1342308352
Control15=IDC_STATIC_PLAY_SPEED,static,1342308352
Control16=IDC_SLIDER_PLAY_SPEED,msctls_trackbar32,1342242840
Control17=IDC_STATIC_PLAY_POSITION,static,1342308352
Control18=IDC_SLIDER_PLAY_POSITION,msctls_trackbar32,1342242840
Control19=IDC_STATIC_GROUP_LEFT,button,1342177287
Control20=IDC_STATIC_GROUP_RIGHT,button,1342177287
Control21=IDC_STATIC_CARDS_NUM,static,1342308352
Control22=IDC_COMB_CARDS_NUM,combobox,1344339971
Control23=IDC_STATIC_PLAY_POS_TIME,static,1342308352

[DLG:IDD_DLG_PROPERTY]
Type=1
Class=CDlgProrertySet
ControlCount=6
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_STATIC,static,1342308352
Control4=IDC_STATIC,static,1342308352
Control5=IDC_SLI_BRIGHTNESS,msctls_trackbar32,1342242840
Control6=IDC_SLI_CONTRAST,msctls_trackbar32,1342242840

[CLS:CDlgProrertySet]
Type=0
HeaderFile=DlgProrertySet.h
ImplementationFile=DlgProrertySet.cpp
BaseClass=CDialog
Filter=D
LastObject=IDC_SLI_BRIGHTNESS
VirtualFilter=dWC

[DLG:IDD_DLG_RESOLUTION]
Type=1
Class=CDlgResolution
ControlCount=4
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_STATIC,static,1342308352
Control4=IDC_COMB_RESOLUTION,combobox,1344339971

[CLS:CDlgResolution]
Type=0
HeaderFile=DlgResolution.h
ImplementationFile=DlgResolution.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=CDlgResolution

[DLG:IDD_DLG_BROWSE]
Type=1
Class=CDlgBrowse
ControlCount=2
Control1=IDC_BTN_UP,button,1476460544
Control2=IDC_BTN_DOWN,button,1342242816

[CLS:CDlgBrowse]
Type=0
HeaderFile=DlgBrowse.h
ImplementationFile=DlgBrowse.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=IDC_BTN_UP

[CLS:CPlayBackup]
Type=0
HeaderFile=PlayBackup.h
ImplementationFile=PlayBackup.cpp
BaseClass=CWnd
Filter=W
VirtualFilter=WC

[CLS:CPlayView]
Type=0
HeaderFile=PlayView.h
ImplementationFile=PlayView.cpp
BaseClass=CWnd
Filter=W
VirtualFilter=WC

