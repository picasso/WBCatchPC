// PlayView.cpp : implementation file
//

#include "stdafx.h"
#include "wbcatch.h"
#include "PlayView.h"
#include "YuvFormat.h"
#include "common.h"
#include "UniqueObject.h"
#include "PostMessage.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define PLYAER_INDEX	0
/////////////////////////////////////////////////////////////////////////////
// CPlayView

CPlayView::CPlayView()
{
	m_nMaxCap = 0;
	m_bIsCapture = FALSE;
	m_tStartCapTime = 0;
	m_nCameraChannel = -1;
}

CPlayView::~CPlayView()
{
}

BOOL CPlayView::CreateWnd( DWORD dwExStyle, DWORD dwStyle, const CRect& rc, CWnd* pParent, UINT uID )
{
	ASSERT( pParent != NULL );
	BOOL bRet = FALSE;
	
	LPCTSTR pszClsName = AfxRegisterWndClass(0, LoadCursor( NULL, MAKEINTRESOURCE(IDC_ARROW) ));
	bRet = CreateEx( dwExStyle, pszClsName, TEXT(""), dwStyle | WS_VSCROLL, rc, pParent, uID );
	if (bRet) ShowScrollBar(SB_VERT, FALSE);
	return bRet;
}

BEGIN_MESSAGE_MAP(CPlayView, CWnd)
	//{{AFX_MSG_MAP(CPlayView)
	ON_WM_PAINT()
	ON_WM_CREATE()
	ON_WM_TIMER()
	ON_MESSAGE(UM_PLAY_FRAMES_IMAGE, OnDrawFrameImage)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CPlayView message handlers

void CPlayView::Quit()
{
	m_bIsCapture = FALSE;
	//Stop();
}

void CPlayView::OnPaint() 
{
	CPaintDC dc(this); // device context for painting
	
	// TODO: Add your message handler code here
	CRect	rcClient;
	GetClientRect(rcClient);
	dc.FillSolidRect(rcClient, RGB(0,0,0));
	// Do not call CWnd::OnPaint() for painting messages
}

int CPlayView::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	// TODO: Add your specialized creation code here
	CPostMessage * pPostMessage = CPostMessage::GetObject();
	pPostMessage->RegisterMessage(m_hWnd, UM_PLAY_FRAMES_IMAGE);

	return 0;
}

////视频数据回调
void CPlayView::UIEventCallBackHandler(MP_ENG_EVENT event, int nIndex, void *pParam, void *pAppData)
{
    if (event == MP_EVENT_DATA_READY)
	{
		CPlayView * pThis = static_cast<CPlayView *>(pAppData);
		MP_DATA_INFO *pData = (MP_DATA_INFO *) pParam;
		if ((pData->type == MP_DATA_YUV) && (pThis))
		{
			MP_YUV_INFO *pYUVData		= (MP_YUV_INFO *) pData->pData;
			DWORD dwSize				= pYUVData->nWidth * pYUVData->nHeight * 3;
			int nWidth					= pYUVData->nWidth;
			int nHeight					= pYUVData->nHeight;
			BYTE * rgb_data				= (BYTE *)malloc(dwSize);
			if (rgb_data)
			{
				yuv2rgb(rgb_data, pYUVData->pY, nWidth, nHeight);
				CUniqueObject * pObject = CUniqueObject::GetObject();
				CPostMessage * pPostMessage = CPostMessage::GetObject();
				//这个里面不能加锁，加锁就出问题，不知道为什么
				RGBIMAGEINFO	info = {0};
				info.dwSize = dwSize;
				info.nHeight = nHeight;
				info.nWidth = nWidth;
				info.pRGB = rgb_data;
				pObject->InsertMemory((LPARAM)rgb_data, info);
				
				//
				HBITMAP hBmp = CreateHandleFromRGBBuffer(info.pRGB, info.dwSize, info.nWidth, info.nHeight, 24);
				if (hBmp)
				{
					HWND	hWnd = pThis->GetSafeHwnd();
					if(::IsWindow(hWnd))
					{
						Draw(hBmp, hWnd);
					}
					::DeleteObject(hBmp);
				}
				//
				pPostMessage->PostMessage(UM_PLAY_FRAMES_IMAGE, 0, (LPARAM)rgb_data);
			}
		}		
	}
}

//检查现在是否在抓捕
BOOL CPlayView::IsCapture()
{
	return m_bIsCapture;
}

//设置抓捕状态
void CPlayView::SetCaptureStatus(BOOL bIsCap)
{
	m_bIsCapture = bIsCap;
	if (m_bIsCapture)
	{
		time(&m_tStartCapTime);
	}
}

//获取当前最大抓捕时机
int CPlayView::GetMaxCapTime()
{
	return m_nMaxCap;
}

//设置最大抓捕时间
void CPlayView::SetMaxCapTime(int nTime)
{
	m_nMaxCap = nTime;
}

//获取开始抓捕时间
time_t CPlayView::GetStartCapTime()
{
	return m_tStartCapTime;
}

//初始化播放设备
void CPlayView::InitDevice()
{
	CPlaybackEngine* pPlayer = CPlaybackEngine::getInstance(PLYAER_INDEX);
	
	pPlayer->SetMode(MP_MODE_DEC_YUVDATA);
	pPlayer->SetCallbackFunc(CPlayView::UIEventCallBackHandler, this);
	pPlayer->SetLantencyTime(1);
	pPlayer->Load();
}

//播放
void CPlayView::Player()
{
	CCameraMngr* pCameraManager = CCameraMngr::getInstance();
	CPlaybackEngine* pPlayer = CPlaybackEngine::getInstance(PLYAER_INDEX);
	if (m_nCameraChannel != -1)
	{
		pCameraManager->AssignPlayer(PLYAER_INDEX, m_nCameraChannel);
	}
	pPlayer->Start();
}

//停止播放
void CPlayView::Stop()
{
	CPlaybackEngine* pPlayer = CPlaybackEngine::getInstance(PLYAER_INDEX);
	CCameraMngr* pCameraManager = CCameraMngr::getInstance();
	pPlayer->Stop();
	if (m_nCameraChannel != -1)
	{
		pCameraManager->AssignPlayer(-1, m_nCameraChannel);
	}
	pPlayer->RefreshWindow();
}

//设置设备通道
void CPlayView::SetCameraChannel(int nCameraChannel)
{
	m_nCameraChannel = nCameraChannel;
}

void CPlayView::OnTimer(UINT nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default
	
	CWnd::OnTimer(nIDEvent);
}

LRESULT CPlayView::OnDrawFrameImage(WPARAM wParam, LPARAM lParam)
{
	CUniqueObject * pUnique = CUniqueObject::GetObject();
	BOOL			bDeleteMemory = TRUE;
	CPostMessage *	pPostMessage = CPostMessage::GetObject();

	pUnique->Lock();
	RGBIMAGEINFO info = pUnique->FindMemory(lParam);
	do 
	{
		if (NULL == info.pRGB) break;
		HBITMAP hBmp = CreateHandleFromRGBBuffer(info.pRGB, info.dwSize, info.nWidth, info.nHeight, 24);
		if (NULL == hBmp) break;
		if (IsCapture())
		{
			time_t			tEndTime;
			time(&tEndTime);
			if (tEndTime - GetStartCapTime() < GetMaxCapTime()){
				int nCount = pUnique->InsertCaptureImage(hBmp);
				pPostMessage->PostMessage(UM_SET_SPEED_TOTAL, nCount, 0);
				bDeleteMemory = FALSE;
			}
			else{
				pPostMessage->PostMessage(UM_STOP_CAPTURE, 0, 0);
			}
		}
		if (bDeleteMemory){
			::DeleteObject(hBmp);
		}
	} while (0);
	pUnique->DeleteMemory(lParam);
	pUnique->UnLock();
	return 0;
}