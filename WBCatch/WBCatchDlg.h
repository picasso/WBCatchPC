// WBCatchDlg.h : header file
//

#if !defined(AFX_WBCATCHDLG_H__2061072A_4ED4_4AAC_8CB1_A98F5B647AB3__INCLUDED_)
#define AFX_WBCATCHDLG_H__2061072A_4ED4_4AAC_8CB1_A98F5B647AB3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
#include "BackupView.h"
#include "PlayView.h"
// CWBCatchDlg dialog

class CWBCatchDlg : public CDialog
{
// Construction
public:
	CWBCatchDlg(CWnd* pParent = NULL);	// standard constructor
// Dialog Data
	//{{AFX_DATA(CWBCatchDlg)
	enum { IDD = IDD_WBCATCH_DIALOG };
	CStatic	m_staPlayPosTime;
	CStatic	m_staCardsNum;
	CComboBox	m_combCardsNum;
	CStatic	m_staPlayer;
	CStatic	m_staPlayers;
	CStatic	m_staPlaySpeed;
	CStatic	m_staPlayPos;
	CStatic	m_staMaxCap;
	CButton	m_btnGroupRight;
	CButton	m_btnGroupLeft;
	CStatic	m_staCamera;
	CStatic	m_staBacupPlay;
	CSpinButtonCtrl	m_spinMaxCap;
	CSliderCtrl	m_sliPlaySpeed;
	CSliderCtrl	m_sliPlayPos;
	CEdit	m_edtMaxCap;
	CComboBox	m_combPlayers;
	CButton	m_btnStopCap;
	CButton	m_btnStartCap;
	CButton	m_btnResolution;
	CButton	m_btnProperty;
	CButton	m_btnBrowse;
	CComboBox	m_combDeviceList;
	BOOL m_bIsCapture;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CWBCatchDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
public:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CWBCatchDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnBtnStartCapture();
	afx_msg void OnBtnStopCapture();
	afx_msg void OnReleasedcaptureSliderPlayPosition(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnReleasedcaptureSliderPlaySpeed(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnBtnProperty();
	afx_msg void OnBtnResolution();
	afx_msg void OnBtnBrowse();
	afx_msg BOOL OnEraseBkgnd( CDC* pDC );
	afx_msg void OnClose();
	afx_msg void OnDeltaposSpinMaxCapture(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnSelchangeCombDeviceList();
	afx_msg void OnSelchangeCombPlayers();
	afx_msg void OnSelchangeCombCardsNum();
	afx_msg LRESULT OnSetSpeedTotal(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSetSpeedPos(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnStopCapture(WPARAM wParam, LPARAM lParam);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg LRESULT OnSetCaptureNumber(WPARAM wParam, LPARAM lParam);
	afx_msg void OnGetMinMaxInfo( MINMAXINFO FAR* lpMMI );
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	//设置玩家数
	void SetPlayer(int nPlayer);
	//设置牌数
	void SetCardsNum(int nCards);
	//设置最大抓捕时间
	void SetMaxCapture(int nPos);
	//设置服务器ID
	void SetCamServerID(int nCamServerID);
	//初始化设备列表
	void InitDeviceList();
	//回放速度发生改变
	void OnChangeBackupSpeed();
	//回放帧位置发生改变
	void OnChangeBackupPos();
	//设置回放总帧数(该值是变动的)
	void SetBackupTotalFrames(int nFrames);
	//设置回放帧位置提示
	void SetBackupPosTip();
	//设置回放帧位置
	void SetBackupPos(int nPos);
	//获取回放帧位置
	int GetBackupPos();
	//设置播放速度
	void SetBackupSpeed(int nPos);
	//设置播放速度提示
	void SetBackupSpeedTip();
	//初始化开始数据
	void Refurbish();
	//获取文本区大小
	CSize GetTextExtent(CString str);
	//退出程序前的销毁工作
	void Quit();
	//检查现在是否在抓捕
	BOOL IsCapture();
	//插入配置信息
	void InsertConfig();
	//设置状态栏文本
	void SetStatusBarText(int nIndex, LPCTSTR szText);
	//读取用户配置
	static int SQLiteConfig(void* pParam, int nCount, char** pValue, char** pName);
private:
	int				m_nCamServerID;
	CBackupView *	m_pBackupView;
	CPlayView *		m_pPlayView;
	CStatusBar		m_wndStatusBar;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_WBCATCHDLG_H__2061072A_4ED4_4AAC_8CB1_A98F5B647AB3__INCLUDED_)
