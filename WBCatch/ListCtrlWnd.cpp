#include "stdAfx.h"
#include "ListCtrlWnd.h"
#include "UniqueObject.h"
#include "PostMessage.h"

CListCtrlWnd::CListCtrlWnd()
{
	memset(&m_ImageList, 0, sizeof(m_ImageList));
	m_nTotalPages = 1;
	m_nCurrentPage = 1;
	m_nItemColumn = 1;
	m_nItemWidth = 20;
	m_nItemHeight = 20;
	m_nCurrentPos = 0;
	m_bHasScroll = FALSE;
	m_Font.CreateFont(10, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("宋体")); 
}

CListCtrlWnd::~CListCtrlWnd()
{
	
}

BEGIN_MESSAGE_MAP( CListCtrlWnd, CWnd )
    ON_WM_PAINT()
    ON_WM_ERASEBKGND()
    ON_WM_VSCROLL()
	ON_WM_MOUSEWHEEL()
	ON_WM_RBUTTONUP()
	ON_WM_LBUTTONUP()
END_MESSAGE_MAP()

BOOL CListCtrlWnd::CreateWnd( DWORD dwExStyle, DWORD dwStyle, const CRect& rc, CWnd* pParent, UINT uID )
{
	ASSERT( pParent != NULL );
	BOOL bRet = FALSE;
	
	LPCTSTR pszClsName = AfxRegisterWndClass(0, LoadCursor( NULL, MAKEINTRESOURCE(IDC_ARROW) ));
	bRet = CreateEx( dwExStyle, pszClsName, TEXT(""), dwStyle | WS_VSCROLL, rc, pParent, uID );
	if (bRet) ShowScrollBar(SB_VERT, FALSE);
	return bRet;
}

void CListCtrlWnd::Init(int nColumn, int nImageCount, int nImageWidth, int ImageHeight)//窗口创建成功后第一个调用
{
	do 
	{
		CRect		rcClient;
		float		fScale = 0.0;
		int			nScrollWidth = GetSystemMetrics(SM_CXVSCROLL);

		if (nImageCount <= 0) break;
		if (!::IsWindow(m_hWnd)) break;
		GetClientRect(rcClient);
		m_nItemWidth = rcClient.Width()/nColumn;
		if (m_nItemWidth == 0) break;
		fScale = (nImageWidth * 1.0)/m_nItemWidth;
		m_nItemHeight = m_nItemWidth;
		if (fScale != 0) m_nItemHeight = ImageHeight/fScale;
		m_nItemColumn = nColumn;
		int nRow = nImageCount/nColumn;
		int nLine = nImageCount%nColumn;
		if (nLine) nRow += 1;
		int nHeight = nRow * m_nItemHeight;
		if (nHeight > rcClient.Height())
		{
			//重新计算下宽高
			m_nItemWidth = (rcClient.Width() - nScrollWidth)/nColumn;
			if (m_nItemWidth == 0) break;
			fScale = (nImageWidth * 1.0)/m_nItemWidth;
			m_nItemHeight = m_nItemWidth;
			if (fScale != 0) m_nItemHeight = ImageHeight/fScale;
			m_nItemColumn = nColumn;
			int nRow = nImageCount/nColumn;
			int nLine = nImageCount%nColumn;
			if(nLine) nRow += 1;
			nHeight = nRow * m_nItemHeight;

			SCROLLINFO scInfo = { 0 };
			scInfo.cbSize = sizeof( SCROLLINFO );
			scInfo.fMask  = SIF_ALL;
			scInfo.nMin   = 0;
			scInfo.nMax   = nHeight;
			scInfo.nPos   = 0;
			scInfo.nPage  = rcClient.Height();
			SetScrollInfo( SB_VERT, &scInfo, TRUE );
			m_bHasScroll = TRUE;	
		}
	} while (0);
}

BOOL CListCtrlWnd::AddItem(HBITMAP hBmp, int nIndex)
{
	BOOL	bRet = FALSE;

	do 
	{
		if (NULL == hBmp) break;
		if (!::IsWindow(m_hWnd)) break;
		int nCount = GetItemCount();
		int nX = nCount/m_nItemColumn;
		int nY = nCount%m_nItemColumn;
		UITEMINFO itemInfo;
		itemInfo.hBmp = hBmp;
		itemInfo.nIndex = nIndex;
		itemInfo.rcItem.left = nY * m_nItemWidth;
		itemInfo.rcItem.right = itemInfo.rcItem.left + m_nItemWidth;
		itemInfo.rcItem.top = m_nCurrentPos + nX * m_nItemHeight;
		itemInfo.rcItem.bottom = itemInfo.rcItem.top + m_nItemHeight;
		m_mapImage.insert(std::make_pair(nCount, itemInfo));
		Invalidate();
		bRet = TRUE;
	} while (0);

	return bRet;
}

int CListCtrlWnd::GetItemCount()
{
	return m_mapImage.size();
}

HBITMAP  CListCtrlWnd::CopyBitmap(HBITMAP  hSourcehBitmap )
{
	CDC sourcedc;
	CDC destdc;
	sourcedc.CreateCompatibleDC(NULL);
	destdc.CreateCompatibleDC(NULL);
	//the   bitmap   information.
	BITMAP   bm = {0};
	//get   the   bitmap   information.
	::GetObject(hSourcehBitmap,   sizeof(bm),   &bm);
	//   create   a   bitmap   to   hold   the   result
	HBITMAP   hbmresult   =   ::CreateCompatibleBitmap(CClientDC(NULL),   bm.bmWidth ,   bm.bmHeight );
	HBITMAP   hbmoldsource   =   (HBITMAP)::SelectObject(   sourcedc.m_hDC ,   hSourcehBitmap);
	HBITMAP   hbmolddest   =   (HBITMAP)::SelectObject(   destdc.m_hDC ,   hbmresult   );
	destdc.BitBlt(0,0,bm.bmWidth ,   bm.bmHeight ,   &sourcedc,   0,   0,   SRCCOPY   );
	
	//   restore   dcs
	::SelectObject(  sourcedc.m_hDC ,   hbmoldsource   );
	::SelectObject(  destdc.m_hDC ,   hbmolddest   );
	::DeleteObject(  sourcedc.m_hDC );
	::DeleteObject(  destdc.m_hDC );
	
	return   hbmresult;
}

Gdiplus::SizeF GetTextBounds(Gdiplus::Font& font, Gdiplus::StringFormat& strFormat,const wchar_t *szText)
{
    Gdiplus::GraphicsPath graphicsPathObj;
    Gdiplus::FontFamily fontfamily;
    font.GetFamily(&fontfamily);
    graphicsPathObj.AddString(szText,-1,&fontfamily,font.GetStyle(),font.GetSize(),\
		Gdiplus::PointF(0,0),&strFormat);
    Gdiplus::RectF rcBound;
    /// 获取边界范围
    graphicsPathObj.GetBounds(&rcBound);
    /// 返回文本的宽高
    return Gdiplus::SizeF(rcBound.Width,rcBound.Height);
}

void CListCtrlWnd::OnPaint()
{
	CPaintDC	dc( this );
	CDC			memDC;
	CRect		rcClient;
	BITMAP		bmpInfo = {0};

#if 0
	::SetViewportOrgEx( dc.GetSafeHdc(), 0, -m_nCurrentPos, NULL );//关键代码
	GetClientRect(&rcClient);
	memDC.CreateCompatibleDC(&dc);
	
	CRect	rcLast;
	CString	strIndex;
	CSize	size;

	for(std::map<int, UITEMINFO>::iterator it = m_mapImage.begin(); it != m_mapImage.end(); it++)
	{
		GetObject(it->second.hBmp,sizeof(BITMAP),(void*)&bmpInfo);
		HBITMAP hCopyBitmap = CopyBitmap(it->second.hBmp);
		if (hCopyBitmap)
		{
			HBITMAP hOldBitmap = (HBITMAP)memDC.SelectObject(hCopyBitmap);
			//绘制索引
			rcLast = it->second.rcItem;
			strIndex.Format(_T("图片%d"), it->second.nIndex + 1);
			memDC.SetBkMode( TRANSPARENT );
			CFont * pOldFont = (CFont *)memDC.SelectObject(&m_Font);
			memDC.SetTextColor(RGB(255,0,0));
			size = memDC.GetTextExtent(strIndex);
			memDC.DrawText(strIndex, CRect(bmpInfo.bmWidth - size.cx, 0, bmpInfo.bmWidth, size.cy), DT_LEFT);
			memDC.SelectObject(pOldFont);
			//绘制图片
			dc.StretchBlt(rcLast.left, rcLast.top, rcLast.Width(), rcLast.Height(), &memDC, 0, 0, bmpInfo.bmWidth, bmpInfo.bmHeight, SRCCOPY);
			memDC.SelectObject(hOldBitmap);
			DeleteObject(hCopyBitmap);
		}
	}
	
	int nImageCount = m_mapImage.size();
	int nLine = nImageCount%m_nItemColumn;
	if (nLine)
	{
		//绘制背景
		rcLast.left = rcLast.right;
		rcLast.right = rcClient.right;
		dc.FillSolidRect(rcLast, GetSysColor(COLOR_3DFACE));//用对话框背景色填充
	}
	memDC.DeleteDC();
#endif
	//新需求的代码
	CBitmap		newBitmap;
	GetClientRect(&rcClient);
	memDC.CreateCompatibleDC(&dc);
	newBitmap.CreateCompatibleBitmap(&dc, rcClient.Width(), rcClient.Height());
	CBitmap * pOldBitmap = (CBitmap *)memDC.SelectObject(&newBitmap);

	memDC.FillSolidRect(rcClient, GetSysColor(COLOR_3DFACE));//用对话框背景色填充
	CRect	rcLast;
	CString	strIndex;
	int		nGDIFont = 12;
	CSize	size;

	if (m_nCards == 2)
	{
		int nRow = m_nCurrentPage - 1;
		int	nCount = 0;
		int nAddCount = 0;
		for(std::map<int, UITEMINFO>::iterator it = m_mapImage.begin(); it != m_mapImage.end(); it++)
		{
			if (nCount == nRow){
				//
				GetObject(it->second.hBmp,sizeof(BITMAP),(void*)&bmpInfo);
				HBITMAP hCopyBitmap = CopyBitmap(it->second.hBmp);
				if (hCopyBitmap)
				{
					Gdiplus::Graphics	Grap(memDC.GetSafeHdc());
					Gdiplus::Bitmap *	pBitmap = Gdiplus::Bitmap::FromHBITMAP(hCopyBitmap, NULL);
					wchar_t				wszIndex[MAX_PATH] = {0};
					if (pBitmap)
					{
						rcLast = it->second.rcItem;
						Gdiplus::RectF		rcDest((Gdiplus::REAL)rcLast.left, (Gdiplus::REAL)rcLast.top, (Gdiplus::REAL)rcLast.Width(), (Gdiplus::REAL)rcLast.Height());
						Grap.DrawImage(pBitmap, rcDest, (Gdiplus::REAL)0, (Gdiplus::REAL)0, (Gdiplus::REAL)bmpInfo.bmWidth, (Gdiplus::REAL)bmpInfo.bmHeight, Gdiplus::UnitPixel);
						wsprintfW(wszIndex, L"图片%d", it->second.nIndex + 1);
						Gdiplus::Font	font(L"宋体", (Gdiplus::REAL)nGDIFont);
						Gdiplus::SolidBrush		brush(Gdiplus::Color(255, 0, 0));
						Gdiplus::StringFormat	format;
						
						format.SetTrimming(Gdiplus::StringTrimmingEllipsisCharacter);
						format.SetAlignment(Gdiplus::StringAlignmentFar);
						Gdiplus::SizeF gdiSize = GetTextBounds(font, format, wszIndex);
						Grap.DrawString(wszIndex, -1, &font, Gdiplus::RectF((Gdiplus::REAL)rcLast.left, (Gdiplus::REAL)rcLast.bottom - gdiSize.Height - 12, (Gdiplus::REAL)rcLast.Width(), (Gdiplus::REAL)gdiSize.Height + 10), &format, &brush);
						
						delete pBitmap;
						pBitmap = NULL;
					}
					DeleteObject(hCopyBitmap);
				}
				//
				nRow = nRow + 3;
				nAddCount++;
			}
			if (nAddCount == 18){
				break;
			}
			nCount++;
		}
	}
	else if (m_nCards == 1)
	{
		int		nIndex = 0;
		BOOL	bStart = FALSE;
		for(std::map<int, UITEMINFO>::iterator it = m_mapImage.begin(); it != m_mapImage.end(); it++)
		{
			if (m_nCurrentPage == 1){
				if (nIndex < 2){
					bStart = TRUE;
				}
				else{
					bStart = FALSE;
					break;
				}
			}
			else if (m_nCurrentPage == 2){
				if (nIndex >= 2){
					bStart = TRUE;
				}
				else{
					bStart = FALSE;
				}
			}
			if (bStart){
				GetObject(it->second.hBmp,sizeof(BITMAP),(void*)&bmpInfo);
				HBITMAP hCopyBitmap = CopyBitmap(it->second.hBmp);
				if (hCopyBitmap)
				{
					Gdiplus::Graphics	Grap(memDC.GetSafeHdc());
					Gdiplus::Bitmap *	pBitmap = Gdiplus::Bitmap::FromHBITMAP(hCopyBitmap, NULL);
					wchar_t				wszIndex[MAX_PATH] = {0};
					if (pBitmap)
					{
						rcLast = it->second.rcItem;
						Gdiplus::RectF		rcDest((Gdiplus::REAL)rcLast.left, (Gdiplus::REAL)rcLast.top, (Gdiplus::REAL)rcLast.Width(), (Gdiplus::REAL)rcLast.Height());
						Grap.DrawImage(pBitmap, rcDest, (Gdiplus::REAL)0, (Gdiplus::REAL)0, (Gdiplus::REAL)bmpInfo.bmWidth, (Gdiplus::REAL)bmpInfo.bmHeight, Gdiplus::UnitPixel);
						wsprintfW(wszIndex, L"图片%d", it->second.nIndex + 1);
						Gdiplus::Font	font(L"宋体", (Gdiplus::REAL)nGDIFont);
						Gdiplus::SolidBrush		brush(Gdiplus::Color(255, 0, 0));
						Gdiplus::StringFormat	format;
						
						format.SetTrimming(Gdiplus::StringTrimmingEllipsisCharacter);
						format.SetAlignment(Gdiplus::StringAlignmentFar);
						Gdiplus::SizeF gdiSize = GetTextBounds(font, format, wszIndex);
						Grap.DrawString(wszIndex, -1, &font, Gdiplus::RectF((Gdiplus::REAL)rcLast.left, (Gdiplus::REAL)rcLast.bottom - gdiSize.Height - 12, (Gdiplus::REAL)rcLast.Width(), (Gdiplus::REAL)gdiSize.Height + 10), &format, &brush);
							
						delete pBitmap;
						pBitmap = NULL;
					}
					DeleteObject(hCopyBitmap);
				}
			}
			nIndex++;
		}
	}
	else if (m_nCards == 0)
	{
		int nStartRow = (m_nCurrentPage - 1) * 4;
		int nEndRow = nStartRow + 3;
		int	nX = 0;
		for(std::map<int, UITEMINFO>::iterator it = m_mapImage.begin(); it != m_mapImage.end(); it++)
		{
			if (nX == m_nItemColumn){
				nX = 0;
			}

			if ((nX >= nStartRow)  && (nX <= nEndRow))
			{
				GetObject(it->second.hBmp,sizeof(BITMAP),(void*)&bmpInfo);
				HBITMAP hCopyBitmap = CopyBitmap(it->second.hBmp);
				if (hCopyBitmap)
				{
					Gdiplus::Graphics	Grap(memDC.GetSafeHdc());
					Gdiplus::Bitmap *	pBitmap = Gdiplus::Bitmap::FromHBITMAP(hCopyBitmap, NULL);
					wchar_t				wszIndex[MAX_PATH] = {0};
					if (pBitmap)
					{
						rcLast = it->second.rcItem;
						Gdiplus::RectF		rcDest((Gdiplus::REAL)rcLast.left, (Gdiplus::REAL)rcLast.top, (Gdiplus::REAL)rcLast.Width(), (Gdiplus::REAL)rcLast.Height());
						Grap.DrawImage(pBitmap, rcDest, (Gdiplus::REAL)0, (Gdiplus::REAL)0, (Gdiplus::REAL)bmpInfo.bmWidth, (Gdiplus::REAL)bmpInfo.bmHeight, Gdiplus::UnitPixel);
						wsprintfW(wszIndex, L"图片%d", it->second.nIndex + 1);
						Gdiplus::Font	font(L"宋体", (Gdiplus::REAL)nGDIFont);
						Gdiplus::SolidBrush		brush(Gdiplus::Color(255, 0, 0));
						Gdiplus::StringFormat	format;
						
						format.SetTrimming(Gdiplus::StringTrimmingEllipsisCharacter);
						format.SetAlignment(Gdiplus::StringAlignmentFar);
						Gdiplus::SizeF gdiSize = GetTextBounds(font, format, wszIndex);
						Grap.DrawString(wszIndex, -1, &font, Gdiplus::RectF((Gdiplus::REAL)rcLast.left, (Gdiplus::REAL)rcLast.bottom - gdiSize.Height - 12, (Gdiplus::REAL)rcLast.Width(), (Gdiplus::REAL)gdiSize.Height + 10), &format, &brush);
						
						delete pBitmap;
						pBitmap = NULL;
					}
					DeleteObject(hCopyBitmap);
				}
			}
			nX++;
		}
	}
	dc.BitBlt(0, 0, rcClient.Width(), rcClient.Height(), &memDC, 0, 0, SRCCOPY);
	memDC.SelectObject(pOldBitmap);
	memDC.DeleteDC();
	newBitmap.DeleteObject();
}

BOOL CListCtrlWnd::OnEraseBkgnd( CDC* pDC )
{
	return TRUE;
}

void CListCtrlWnd::OnVScroll( UINT nSBCode, UINT nPosX, CScrollBar* pScrollBar )
{
	CWnd::OnVScroll( nSBCode, nPosX, pScrollBar );
	SCROLLINFO srInfo = { 0 };
	int	nPos = 0;
	srInfo.cbSize = sizeof( SCROLLINFO );
	srInfo.fMask  = SIF_ALL;
	GetScrollInfo( SB_VERT, &srInfo );
	switch( nSBCode )
	{
	case SB_LINEUP://向上滚动一行
	case SB_PAGEUP:
		{
			nPos = m_nCurrentPos - m_nItemHeight;
			if (nPos > srInfo.nMin)
			{
				ScrollWindow( 0, m_nItemHeight );
				m_nCurrentPos = nPos;
				SetScrollPos( SB_VERT, m_nCurrentPos, TRUE );
			}
			else
			{
				ScrollWindow( 0, m_nCurrentPos - srInfo.nMin);
				m_nCurrentPos = srInfo.nMin;
				SetScrollPos( SB_VERT, m_nCurrentPos, TRUE );
			}
		}
		break;
	case SB_LINEDOWN:
	case SB_PAGEDOWN:
		{
			nPos = m_nCurrentPos + m_nItemHeight;
			if (nPos < srInfo.nMax - srInfo.nPage)
			{
				ScrollWindow( 0, -m_nItemHeight );
				m_nCurrentPos = nPos;
				SetScrollPos( SB_VERT, m_nCurrentPos, TRUE );
			}
			else
			{
				ScrollWindow( 0, -(srInfo.nMax - srInfo.nPage - m_nCurrentPos) );
				m_nCurrentPos = srInfo.nMax - srInfo.nPage;
				SetScrollPos( SB_VERT, m_nCurrentPos, TRUE );
			}
		}
		break;
	case SB_THUMBPOSITION:
		{
			SCROLLINFO srInfo = { 0 };
			srInfo.cbSize = sizeof( SCROLLINFO );
			srInfo.fMask  = SIF_ALL;
			GetScrollInfo( SB_VERT, &srInfo );
			int nTrackpos = srInfo.nTrackPos;
			ScrollWindow( 0, m_nCurrentPos - nTrackpos );
			m_nCurrentPos = nTrackpos;
			SetScrollPos( SB_VERT, m_nCurrentPos, TRUE );
		}
		break;
	default:
		break;
	}
	Invalidate(TRUE);
}

BOOL CListCtrlWnd::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt) 
{
	// TODO: Add your message handler code here and/or call default

	return CWnd::OnMouseWheel(nFlags, zDelta, pt);
}

void CListCtrlWnd::OnRButtonUp(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	if (m_nCards == 0)
	{
		int nStartRow = (m_nCurrentPage - 1) * 4;
		int nEndRow = nStartRow + 3;
		int	nX = 0;
		CRect	rcItem;
		for(std::map<int, UITEMINFO>::iterator it = m_mapImage.begin(); it != m_mapImage.end(); it++)
		{
			if (nX == m_nItemColumn){
				nX = 0;
			}
			if ((nX >= nStartRow)  && (nX <= nEndRow))
			{
				rcItem = it->second.rcItem;
				if (PtInRect(&rcItem, point))
				{
					CUniqueObject * pObject = CUniqueObject::GetObject();
					pObject->Lock();
					int nIndexImage = it->second.nIndex + 1;
					HBITMAP hBitmap = pObject->FindCaptureImage(nIndexImage);
					if (hBitmap)
					{
						it->second.nIndex = nIndexImage;
						it->second.hBmp = hBitmap;
					}
					pObject->UnLock();
					InvalidateRect(rcItem);
					break;
				}
			}
			nX++;
		}
	}
	else if (m_nCards == 1)
	{
		int		nIndex = 0;
		BOOL	bStart = FALSE;
		CRect	rcItem;
		for(std::map<int, UITEMINFO>::iterator it = m_mapImage.begin(); it != m_mapImage.end(); it++)
		{
			if (m_nCurrentPage == 1){
				if (nIndex < 2){
					bStart = TRUE;
				}
				else{
					bStart = FALSE;
					break;
				}
			}
			else if (m_nCurrentPage == 2){
				if (nIndex >= 2){
					bStart = TRUE;
				}
				else{
					bStart = FALSE;
				}
			}
			if (bStart){
				rcItem = it->second.rcItem;
				if (PtInRect(&rcItem, point))
				{
					CUniqueObject * pObject = CUniqueObject::GetObject();
					pObject->Lock();
					int nIndexImage = it->second.nIndex + 1;
					HBITMAP hBitmap = pObject->FindCaptureImage(nIndexImage);
					if (hBitmap)
					{
						it->second.nIndex = nIndexImage;
						it->second.hBmp = hBitmap;
					}
					pObject->UnLock();
					InvalidateRect(rcItem);
					break;
				}
			}
			nIndex++;
		}
	}
	else if (m_nCards == 2)
	{
		int nRow = m_nCurrentPage - 1;
		int	nCount = 0;
		int nAddCount = 0;
		CRect	rcItem;
		for(std::map<int, UITEMINFO>::iterator it = m_mapImage.begin(); it != m_mapImage.end(); it++)
		{
			if (nCount == nRow){
				rcItem = it->second.rcItem;
				if (PtInRect(&rcItem, point))
				{
					CUniqueObject * pObject = CUniqueObject::GetObject();
					pObject->Lock();
					int nIndexImage = it->second.nIndex + 1;
					HBITMAP hBitmap = pObject->FindCaptureImage(nIndexImage);
					if (hBitmap)
					{
						it->second.nIndex = nIndexImage;
						it->second.hBmp = hBitmap;
					}
					pObject->UnLock();
					InvalidateRect(rcItem);
					break;
				}
				nRow = nRow + 3;
				nAddCount++;
			}
			if (nAddCount == 18){
				break;
			}
			nCount++;
		}
	}
	CWnd::OnRButtonUp(nFlags, point);
}

void CListCtrlWnd::OnLButtonUp(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	if (m_nCards == 0)
	{
		int nStartRow = (m_nCurrentPage - 1) * 4;
		int nEndRow = nStartRow + 3;
		int	nX = 0;
		CRect	rcItem;
		for(std::map<int, UITEMINFO>::iterator it = m_mapImage.begin(); it != m_mapImage.end(); it++)
		{
			if (nX == m_nItemColumn){
				nX = 0;
			}
			if ((nX >= nStartRow)  && (nX <= nEndRow))
			{
				rcItem = it->second.rcItem;
				if (PtInRect(&rcItem, point))
				{
					CUniqueObject * pObject = CUniqueObject::GetObject();
					pObject->Lock();
					int nIndexImage = it->second.nIndex - 1;
					HBITMAP hBitmap = pObject->FindCaptureImage(nIndexImage);
					if (hBitmap)
					{
						it->second.nIndex = nIndexImage;
						it->second.hBmp = hBitmap;
					}
					pObject->UnLock();
					InvalidateRect(rcItem);
					break;
				}
			}
			nX++;
		}
	}
	else if (m_nCards == 1)
	{
		int		nIndex = 0;
		BOOL	bStart = FALSE;
		CRect	rcItem;
		for(std::map<int, UITEMINFO>::iterator it = m_mapImage.begin(); it != m_mapImage.end(); it++)
		{
			if (m_nCurrentPage == 1){
				if (nIndex < 2){
					bStart = TRUE;
				}
				else{
					bStart = FALSE;
					break;
				}
			}
			else if (m_nCurrentPage == 2){
				if (nIndex >= 2){
					bStart = TRUE;
				}
				else{
					bStart = FALSE;
				}
			}
			if (bStart){
				rcItem = it->second.rcItem;
				if (PtInRect(&rcItem, point))
				{
					CUniqueObject * pObject = CUniqueObject::GetObject();
					pObject->Lock();
					int nIndexImage = it->second.nIndex - 1;
					HBITMAP hBitmap = pObject->FindCaptureImage(nIndexImage);
					if (hBitmap)
					{
						it->second.nIndex = nIndexImage;
						it->second.hBmp = hBitmap;
					}
					pObject->UnLock();
					InvalidateRect(rcItem);
					break;
				}
			}
			nIndex++;
		}
	}
	else if (m_nCards == 2)
	{
		int nRow = m_nCurrentPage - 1;
		int	nCount = 0;
		int nAddCount = 0;
		CRect	rcItem;
		for(std::map<int, UITEMINFO>::iterator it = m_mapImage.begin(); it != m_mapImage.end(); it++)
		{
			if (nCount == nRow){
				rcItem = it->second.rcItem;
				if (PtInRect(&rcItem, point))
				{
					CUniqueObject * pObject = CUniqueObject::GetObject();
					pObject->Lock();
					int nIndexImage = it->second.nIndex - 1;
					HBITMAP hBitmap = pObject->FindCaptureImage(nIndexImage);
					if (hBitmap)
					{
						it->second.nIndex = nIndexImage;
						it->second.hBmp = hBitmap;
					}
					pObject->UnLock();
					InvalidateRect(rcItem);
					break;
				}
				nRow = nRow + 3;
				nAddCount++;
			}
			if (nAddCount == 18){
				break;
			}
			nCount++;
		}
	}
	CWnd::OnLButtonUp(nFlags, point);
}

void CListCtrlWnd::Scroll(short zDelta)
{
	if (m_bHasScroll)
	{
		SCROLLINFO srInfo = { 0 };
		int	nPos = 0;
		srInfo.cbSize = sizeof( SCROLLINFO );
		srInfo.fMask  = SIF_ALL;
		GetScrollInfo( SB_VERT, &srInfo );

		if (zDelta < 0)
		{
			nPos = m_nCurrentPos + m_nItemHeight;
			if (nPos < srInfo.nMax - srInfo.nPage)
			{
				ScrollWindow( 0, -m_nItemHeight );
				m_nCurrentPos = nPos;
				SetScrollPos( SB_VERT, m_nCurrentPos, TRUE );
			}
			else
			{
				ScrollWindow( 0, -(srInfo.nMax - srInfo.nPage - m_nCurrentPos) );
				m_nCurrentPos = srInfo.nMax - srInfo.nPage;
				SetScrollPos( SB_VERT, m_nCurrentPos, TRUE );
			}//////////////////////////////////////////////////////////////////////////
		}
		else
		{
			nPos = m_nCurrentPos - m_nItemHeight;
			if (nPos > srInfo.nMin)
			{
				ScrollWindow( 0, m_nItemHeight );
				m_nCurrentPos = nPos;
				SetScrollPos( SB_VERT, m_nCurrentPos, TRUE );
			}
			else
			{
				ScrollWindow( 0, m_nCurrentPos - srInfo.nMin);
				m_nCurrentPos = srInfo.nMin;
				SetScrollPos( SB_VERT, m_nCurrentPos, TRUE );
			}//////////////////////////////////////////////////////////////////////////
		}
	}
}

//////////////////////////新处理////////////////////////////////////////////////
void CListCtrlWnd::InitEx(int nCards, int nImageCount, int nImageWidth, int nImageHeight)
{
	if (nCards == 0)//金花
	{
		//金花是固定三行，列是不确定的，单页最多显示4列(必须是3的倍数)
		CRect	rcClient;
		GetClientRect(&rcClient);
		m_nItemHeight = rcClient.Height()/3;
		m_nItemWidth = rcClient.Width()/4;
		m_nTotalPages = nImageCount/12;//每页
		if (nImageCount%12)m_nTotalPages++;
		m_nCurrentPage = 1;
		m_nItemColumn = nImageCount/3;
	}
	else if (nCards == 1)//牌9
	{
		//牌9只显示两行一列，总共最多4张牌(必须4张)
		CRect	rcClient;
		GetClientRect(&rcClient);
		m_nItemWidth = rcClient.Width();
		m_nItemHeight = rcClient.Height()/2;
		m_nTotalPages = 2;
		m_nCurrentPage = 1;
		m_nItemColumn = 1;
	}
	else if (nCards == 2)//跑得快
	{
		CRect	rcClient;
		GetClientRect(&rcClient);
		m_nItemWidth = rcClient.Width()/6;
		m_nItemHeight = rcClient.Height()/3;
		m_nTotalPages = 3;
		m_nCurrentPage = 1;
		m_nItemColumn = 1;
	}
	if (m_nTotalPages > 1)
	{
		CPostMessage * pPostMessage = CPostMessage::GetObject();
		pPostMessage->PostMessage(UM_UP_DOWN, 0, 1);
	}
	else
	{
		CPostMessage * pPostMessage = CPostMessage::GetObject();
		pPostMessage->PostMessage(UM_UP_DOWN, 0, 0);
	}
}

void CListCtrlWnd::OnSize(CRect	rcClient)
{
	if (m_nCards == 0)//金花
	{
		//金花是固定三行，列是不确定的，单页最多显示4列(必须是3的倍数)
		m_nItemHeight = rcClient.Height()/3;
		m_nItemWidth = rcClient.Width()/4;
		
		int nCount = 0;
		for(std::map<int, UITEMINFO>::iterator it = m_mapImage.begin(); it != m_mapImage.end(); it++)
		{
			int nX = nCount%m_nItemColumn;
			int	nY = nCount/m_nItemColumn;
			if (m_nItemColumn > 4){
				nX = nX%4;
			}
			it->second.rcItem.left = nX * m_nItemWidth;
			it->second.rcItem.right = it->second.rcItem.left + m_nItemWidth;
			it->second.rcItem.top = nY * m_nItemHeight;
			it->second.rcItem.bottom = it->second.rcItem.top + m_nItemHeight;
			nCount++;
		}
	}
	else if (m_nCards == 1)//牌9
	{
		//牌9只显示两行一列，总共最多4张牌(必须4张)
		m_nItemWidth = rcClient.Width();
		m_nItemHeight = rcClient.Height()/2;

		int nCount = 0;
		for(std::map<int, UITEMINFO>::iterator it = m_mapImage.begin(); it != m_mapImage.end(); it++)
		{
			int nPage =	nCount%2;
			it->second.rcItem.left = 0;
			it->second.rcItem.right = it->second.rcItem.left + m_nItemWidth;
			it->second.rcItem.top = nPage * m_nItemHeight;
			it->second.rcItem.bottom = it->second.rcItem.top + m_nItemHeight;
			nCount++;
		}
	}
	else if (m_nCards == 2)
	{
		m_nItemHeight = rcClient.Height()/3;
		m_nItemWidth = rcClient.Width()/6;
		int nCount = 0;
		for(std::map<int, UITEMINFO>::iterator it = m_mapImage.begin(); it != m_mapImage.end(); it++)
		{
			int	nY = nCount/18;
			int nRemainder = nCount%18;
			int nX = nRemainder/3;

			it->second.rcItem.left = nX * m_nItemWidth;
			it->second.rcItem.right = it->second.rcItem.left + m_nItemWidth;
			it->second.rcItem.top = nY * m_nItemHeight;
			it->second.rcItem.bottom = it->second.rcItem.top + m_nItemHeight;
			nCount++;
		}
	}
}

BOOL CListCtrlWnd::AddItemEx(HBITMAP hBmp, int nIndex)
{
	BOOL	bRet = FALSE;
	
	do 
	{
		if (NULL == hBmp) break;
		if (!::IsWindow(m_hWnd)) break;
		int nCount = GetItemCount();
		if (m_nCards == 0)//金花
		{
			int nX = nCount%m_nItemColumn;
			int	nY = nCount/m_nItemColumn;
			if (m_nItemColumn > 4){
				nX = nX%4;
			}
			UITEMINFO itemInfo;
			itemInfo.hBmp = hBmp;
			itemInfo.nIndex = nIndex;
			itemInfo.rcItem.left = nX * m_nItemWidth;
			itemInfo.rcItem.right = itemInfo.rcItem.left + m_nItemWidth;
			itemInfo.rcItem.top = nY * m_nItemHeight;
			itemInfo.rcItem.bottom = itemInfo.rcItem.top + m_nItemHeight;
			m_mapImage.insert(std::make_pair(nCount, itemInfo));
			Invalidate();
			bRet = TRUE;
		}
		else if (m_nCards == 1)//牌9
		{
			int nPage =	nCount%2;
			UITEMINFO itemInfo;
			itemInfo.hBmp = hBmp;
			itemInfo.nIndex = nIndex;
			itemInfo.rcItem.left = 0;
			itemInfo.rcItem.right = itemInfo.rcItem.left + m_nItemWidth;
			itemInfo.rcItem.top = nPage * m_nItemHeight;
			itemInfo.rcItem.bottom = itemInfo.rcItem.top + m_nItemHeight;
			m_mapImage.insert(std::make_pair(nCount, itemInfo));
			Invalidate();
			bRet = TRUE;
		}
		else if (m_nCards == 2)//跑得快
		{
			int	nY = nCount/18;
			int nRemainder = nCount%18;
			int nX = nRemainder/3;
	
			UITEMINFO itemInfo;
			itemInfo.hBmp = hBmp;
			itemInfo.nIndex = nIndex;
			itemInfo.rcItem.left = nX * m_nItemWidth;
			itemInfo.rcItem.right = itemInfo.rcItem.left + m_nItemWidth;
			itemInfo.rcItem.top = nY * m_nItemHeight;
			itemInfo.rcItem.bottom = itemInfo.rcItem.top + m_nItemHeight;
			m_mapImage.insert(std::make_pair(nCount, itemInfo));
			Invalidate();
			bRet = TRUE;
		}
	} while (0);
	
	return bRet;
}

void CListCtrlWnd::OnUpPage()
{
	if (m_nCurrentPage > 1)
	{
		m_nCurrentPage--;
		CPostMessage * pPostMessage = CPostMessage::GetObject();
		if (m_nCurrentPage > 1){
			pPostMessage->PostMessage(UM_UP_DOWN, 1, 1);
		}
		else{
			pPostMessage->PostMessage(UM_UP_DOWN, 0, 1);
		}
		Invalidate();
	}
}

void CListCtrlWnd::OnDownPage()
{
	if (m_nCurrentPage < m_nTotalPages)
	{
		m_nCurrentPage++;
		CPostMessage * pPostMessage = CPostMessage::GetObject();
		if (m_nCurrentPage < m_nTotalPages){
			pPostMessage->PostMessage(UM_UP_DOWN, 1, 1);
		}
		else{
			pPostMessage->PostMessage(UM_UP_DOWN, 1, 0);
		}
		Invalidate();
	}
}