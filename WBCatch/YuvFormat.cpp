#include "stdafx.h"
#include "YuvFormat.h"

/************************************************************************/
/*函数功能：初始化BMP文件头                                             */
/*参数：dwImageSize图片数据大小(不包含文件头部分的数据)                 */
/************************************************************************/
BITMAPFILEHEADER InitBmpFileHeader(DWORD dwImageSize)
{
	BITMAPFILEHEADER header;
	::ZeroMemory(&header, sizeof(BITMAPFILEHEADER));
	header.bfType = 0x4d42;
	header.bfSize = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER) + dwImageSize;
	header.bfReserved1 = 0;
	header.bfReserved2 = 0;
	header.bfOffBits = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER);
	return header;
}

/************************************************************************/
/*函数功能：初始化BMP文件头信息头                                       */
/*参数：dwImageSize图片数据大小(不包含文件头部分的数据)                 */
/*      lWidth图片宽带                                                  */
/*      lHeight图片高度                                                 */
/*      wBitcolor色位(取值1,4,8,24,32)                                  */
/************************************************************************/
BITMAPINFOHEADER InitBmpInfoHeader(long lWidth, long lHeight, DWORD dwImageSize, WORD wBitcolor)
{
	BITMAPINFOHEADER bmiHeader;
	::ZeroMemory(&bmiHeader, sizeof(BITMAPINFOHEADER));
	bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	bmiHeader.biWidth = lWidth;
	bmiHeader.biHeight = lHeight;
	bmiHeader.biPlanes = 1;
	bmiHeader.biBitCount = 24;//常用的值为1(黑白二色图), 4(16色图), 8(256色), 24(真彩色图),32位色
	bmiHeader.biCompression = BI_RGB;
	bmiHeader.biSizeImage = dwImageSize;
	bmiHeader.biXPelsPerMeter = 0;
	bmiHeader.biYPelsPerMeter = 0;
	bmiHeader.biClrUsed = 0;
	bmiHeader.biClrImportant = 0;
	return bmiHeader;
}

/************************************************************************/
/*函数功能：rgb数据转bmp文件数据                                        */
/*参数：pRGB RGB图片数据                                                */
/*      lWidth图片宽带                                                  */
/*      lHeight图片高度                                                 */
/*      wBitcolor色位(取值1,4,8,24,32)                                  */
/************************************************************************/
BYTE * rgb_bmp(BYTE * pRGB, long lWidth, long lHeight, DWORD dwImageSize, WORD wBitcolor)
{
	BYTE *						pRet = NULL;
	BITMAPINFOHEADER			bmiHeader = InitBmpInfoHeader(lWidth, lHeight, dwImageSize, wBitcolor);
	BITMAPFILEHEADER			header = InitBmpFileHeader(dwImageSize);
	DWORD						dwLen = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER) + dwImageSize;
	BYTE *						pBuffer = (BYTE *)malloc(dwLen);

	if (pBuffer)
	{
		pRet = pBuffer;
		memcpy(pBuffer, &header, sizeof(header));
		pBuffer = pBuffer + sizeof(header); 
		memcpy(pBuffer, &bmiHeader, sizeof(bmiHeader));
		pBuffer = pBuffer + sizeof(bmiHeader); 
		memcpy(pBuffer, pBuffer, dwImageSize);
	}
	return pRet;
}

/************************************************************************/
/*函数功能：从RGB图片内存中转换HBITMAP句柄                              */
/*参数：pRGB图片内存                                                    */
/*      dwBufferSize图片内存大小                                        */
/*      lWidth图片宽带                                                  */
/*      lHeight图片高度                                                 */
/*      wBitcolor色位(取值1,4,8,24,32)                                  */
/************************************************************************/
HBITMAP CreateHandleFromRGBBuffer(BYTE * pRGB, DWORD dwBufferSize, long lWidth, long lHeight, WORD wBitcolor)
{
	BITMAPINFO bmi;
    ::ZeroMemory(&bmi, sizeof(BITMAPINFO));
    bmi.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
    bmi.bmiHeader.biWidth = lWidth;
    bmi.bmiHeader.biHeight = lHeight;
    bmi.bmiHeader.biPlanes = 1;
    bmi.bmiHeader.biBitCount = wBitcolor;
    bmi.bmiHeader.biCompression = BI_RGB;
    bmi.bmiHeader.biSizeImage = ((lWidth * wBitcolor + 31) / 32) * 4 * lHeight;
	
    LPBYTE pDest = NULL;
    HBITMAP hBmp = ::CreateDIBSection(NULL, &bmi, DIB_RGB_COLORS, (void**)&pDest, NULL, 0);
	if (pRGB)
	{
		memcpy(pDest, pRGB, dwBufferSize);
	}
	return hBmp;
}

unsigned char clip(int valid)
{
	return valid<0 ? 0 : valid>255 ? 255 : valid;
}

//yuv转rgb
void yuv2rgb(unsigned char *rgbout,unsigned char *yuvin, int width,int height)
{
	int temp = 0;
    unsigned long  idx = 0;
    unsigned char *ybase,*ubase,*vbase;
    unsigned char Y,U,V;
    ybase = yuvin;
    ubase = ybase+width*height;
    vbase = ubase+(width*height)/4; 
	for(int y = 0; y<height; y++)
	{
		idx = (height-y-1)*width*3;//该值保证所生成的rgb数据逆序存放在rgbbuf中
        for(int x = 0; x<width; x++)
		{
			Y = ybase[y*width+x];
			U = ubase[(y/2)*(width/2)+x/2];//注意UV分量的取值方法，y/2的含义
			V = vbase[(y/2)*(width/2)+x/2];
			
			temp = (int)(Y+1.771*(U - 128));
			rgbout[idx++] = clip(temp);
			
			temp = (int)(Y-0.7145*(V - 128)-0.3456*(U - 128));
			rgbout[idx++] = clip(temp);
			
			temp = (int)(Y+ 1.4022*(V - 128));
			rgbout[idx++] = clip(temp);
        }
	}
}