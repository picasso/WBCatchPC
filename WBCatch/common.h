#ifndef __COMMON_H__
#define __COMMON_H__

//获取应用程序所在目录
LPCTSTR GetAppPth();

//字符串格式化为UTF8格式
std::string FormatUTF8(LPCTSTR szStr);

//多字节转宽字符
std::wstring FormatUnicode(LPCSTR szStr);

//保存图片
BOOL  SaveBmp(HBITMAP hBitmap, LPCTSTR szFileName);

//绘制图片
void Draw(HBITMAP hBitmap, HWND hWnd);

#endif