// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#if !defined(AFX_STDAFX_H__9C4B82BB_9645_437C_B708_049430E01EC8__INCLUDED_)
#define AFX_STDAFX_H__9C4B82BB_9645_437C_B708_049430E01EC8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions
#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT


//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__9C4B82BB_9645_437C_B708_049430E01EC8__INCLUDED_)

#include <comdef.h>//初始化一下com口
#ifndef ULONG_PTR
#define ULONG_PTR unsigned long*
#include "GdiPlus.h"
#endif
#pragma comment(lib,"gdiplus.lib")

#pragma warning(disable:4786)
#include <string>
#include <list>
#include <map>

#if 0
#include "vld.h"
#endif

#if 1
#define MODE_NETWORK_SERVER 

#include "lib/ControlCommand.h"
#include "lib/PlaybackEngine.h"

#pragma comment(lib, "lib/ControlCommand.lib")
#pragma comment(lib, "lib/PlaybackEngine.lib")
#endif

#if 1
#include "lib/sqlite3.h"

#pragma comment(lib, "lib/sqlite3.lib")
#endif

//message define
#define UM_SET_SPEED_POS		(WM_USER + 1)
#define UM_SET_SPEED_TOTAL		(WM_USER + 2)
#define UM_PLAY_FRAMES_IMAGE	(WM_USER + 3)
#define UM_BACKUP_FRAMES_IMAGE	(WM_USER + 4)
#define UM_STOP_CAPTURE			(WM_USER + 5)
#define UM_CAPTURE_NUMBER		(WM_USER + 6)
#define UM_UP_DOWN				(WM_USER + 7)
