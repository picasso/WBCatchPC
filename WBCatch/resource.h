//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by WBCatch.rc
//
#define IDS_STR_CAPTURE_TIME            1
#define IDS_STR_SELECT_IMAGE            2
#define IDS_STR_LEFT                    3
#define IDD_DLG_LOGIN                   101
#define IDD_WBCATCH_DIALOG              102
#define IDR_MAINFRAME                   128
#define IDD_DLG_PROPERTY                130
#define IDD_DLG_RESOLUTION              131
#define IDD_DLG_BROWSE                  132
#define IDC_EDT_USERNAME                1000
#define IDC_EDT_PWD                     1001
#define IDC_EDT_SERVER                  1002
#define IDC_EDT_PORT                    1003
#define IDC_BTN_LOGIN                   1004
#define IDC_BTN_CANCEL                  1005
#define IDC_CHE_REMEMBERPWD             1007
#define IDC_STATIC_VIDEO                1008
#define IDC_STATIC_BACKPLAY             1009
#define IDC_COMB_DEVICE_LIST            1012
#define IDC_FRAME_BACKUP                1014
#define IDC_BTN_START_CAPTURE           1015
#define IDC_BTN_STOP_CAPTURE            1016
#define IDC_BTN_BROWSE                  1017
#define IDC_BTN_PROPERTY                1018
#define IDC_BTN_RESOLUTION              1019
#define IDC_FRAME_PLAYERVIDEO           1020
#define IDC_COMB_PLAYERS                1021
#define IDC_EDT_MAX_CAPTURE             1022
#define IDC_SPIN_MAX_CAPTURE            1023
#define IDC_STATIC_PLAY_SPEED           1024
#define IDC_SLIDER_PLAY_SPEED           1025
#define IDC_STATIC_PLAY_POSITION        1026
#define IDC_SLIDER_PLAY_POSITION        1027
#define IDC_STATIC_GROUP_LEFT           1028
#define IDC_STATIC_GROUP_RIGHT          1029
#define IDC_STATIC_PLAYERS              1030
#define IDC_STATIC_MAX_CAPTURE          1031
#define IDC_STATIC_CAMERA               1032
#define IDC_STATIC_CARDS_NUM            1033
#define IDC_COMB_CARDS_NUM              1034
#define IDC_COMB_ORDER                  1035
#define IDC_STATIC_PLAY_POS_TIME        1036
#define IDC_STATIC_SPEED_TIME           1037
#define IDC_SLI_BRIGHTNESS              1038
#define IDC_SLI_CONTRAST                1039
#define IDC_COMB_RESOLUTION             1041
#define IDC_LIST                        1042
#define IDC_STATIC_ORDER                1043
#define IDC_BTN_UP                      1044
#define IDC_BTN_DOWN                    1045

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        133
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1045
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
