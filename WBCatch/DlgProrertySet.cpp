// DlgProrertySet.cpp : implementation file
//

#include "stdafx.h"
#include "wbcatch.h"
#include "DlgProrertySet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDlgProrertySet dialog


CDlgProrertySet::CDlgProrertySet(int nCamServerID, CWnd* pParent /*=NULL*/)
	: CDialog(CDlgProrertySet::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgProrertySet)
	m_nBrightness = 0;
	m_nContrast = 0;
	m_nCamServerID = nCamServerID;
	//}}AFX_DATA_INIT
}


void CDlgProrertySet::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgProrertySet)
	DDX_Control(pDX, IDC_SLI_CONTRAST, m_sliContrast);
	DDX_Control(pDX, IDC_SLI_BRIGHTNESS, m_sliBrightness);
	DDX_Slider(pDX, IDC_SLI_BRIGHTNESS, m_nBrightness);
	DDX_Slider(pDX, IDC_SLI_CONTRAST, m_nContrast);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgProrertySet, CDialog)
	//{{AFX_MSG_MAP(CDlgProrertySet)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDlgProrertySet message handlers
//设置亮度
void CDlgProrertySet::SetBrightness(int nBrightness)
{
	CHAR	szCmd[512] = {0};
	CCameraMngr* pCameraManager = CCameraMngr::getInstance();
	int nRet = pCameraManager->SendControlCmd(m_nCamServerID, szCmd);
	if (nRet == -1)
	{
		AfxMessageBox(_T("亮度调整失败！"));
	}
	else
	{
		m_nBrightness = nBrightness;
	}
}

//设置对比度
void CDlgProrertySet::SetContrast(int nContrast)
{
	CHAR	szCmd[512] = {0};
	CCameraMngr* pCameraManager = CCameraMngr::getInstance();
	int nRet = pCameraManager->SendControlCmd(m_nCamServerID, szCmd);
	if (nRet == -1)
	{
		AfxMessageBox(_T("对比度调整失败！"));
	}
	else
	{
		m_nContrast = nContrast;
	}
}
