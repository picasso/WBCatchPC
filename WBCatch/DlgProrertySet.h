#if !defined(AFX_DLGPRORERTYSET_H__EAA98B6E_627F_4F05_9D89_AA2E34261B05__INCLUDED_)
#define AFX_DLGPRORERTYSET_H__EAA98B6E_627F_4F05_9D89_AA2E34261B05__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgProrertySet.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDlgProrertySet dialog

class CDlgProrertySet : public CDialog
{
// Construction
public:
	void SetContrast(int nContrast);
	void SetBrightness(int nBrightness);
	CDlgProrertySet(int nCamServerID, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDlgProrertySet)
	enum { IDD = IDD_DLG_PROPERTY };
	CSliderCtrl	m_sliContrast;
	CSliderCtrl	m_sliBrightness;
	int		m_nBrightness;
	int		m_nContrast;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDlgProrertySet)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDlgProrertySet)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	int m_nCamServerID;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGPRORERTYSET_H__EAA98B6E_627F_4F05_9D89_AA2E34261B05__INCLUDED_)
