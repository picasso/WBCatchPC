// PlayBackup.cpp : implementation file
//

#include "stdafx.h"
#include "wbcatch.h"
#include "BackupView.h"
#include "UniqueObject.h"
#include "YuvFormat.h"
#include "common.h"
#include "PostMessage.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CBackupView

CBackupView::CBackupView()
{
	m_nSpeed = 0;
	m_hThread = NULL;
	m_bIsCapture = FALSE;
	m_nPlayer = 0;
	m_nCardsNum = 0;
	m_nBackupPos = 0;
	m_bRunning = TRUE;
}

CBackupView::~CBackupView()
{
}

BOOL CBackupView::CreateWnd( DWORD dwExStyle, DWORD dwStyle, const CRect& rc, CWnd* pParent, UINT uID )
{
	ASSERT( pParent != NULL );
	BOOL bRet = FALSE;
	
	LPCTSTR pszClsName = AfxRegisterWndClass(0, LoadCursor( NULL, MAKEINTRESOURCE(IDC_ARROW) ));
	bRet = CreateEx( dwExStyle, pszClsName, TEXT(""), dwStyle | WS_VSCROLL, rc, pParent, uID );
	if (bRet) ShowScrollBar(SB_VERT, FALSE);
	return bRet;
}

BEGIN_MESSAGE_MAP(CBackupView, CWnd)
	//{{AFX_MSG_MAP(CBackupView)
	ON_WM_PAINT()
	ON_WM_CREATE()
	ON_WM_TIMER()
	ON_WM_LBUTTONUP()
	ON_WM_LBUTTONDOWN()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CBackupView message handlers

void CBackupView::OnPaint() 
{
	CPaintDC dc(this); // device context for painting
	
	// TODO: Add your message handler code here
	CRect	rcClient;
	GetClientRect(rcClient);
	dc.FillSolidRect(rcClient, RGB(0,0,0));
	// Do not call CWnd::OnPaint() for painting messages
}

int CBackupView::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	// TODO: Add your specialized creation code here
	CPostMessage * pPostMessae = CPostMessage::GetObject();
	pPostMessae->RegisterMessage(m_hWnd, UM_BACKUP_FRAMES_IMAGE);
	m_hThread = ::CreateThread(0, 0, PlayProc, this, 0, 0);
	return 0;
}

DWORD WINAPI CBackupView::PlayProc(LPVOID pParameter)
{
	CBackupView * pThis = static_cast<CBackupView *>(pParameter);
	CPostMessage *pPostMessage = CPostMessage::GetObject();
	while(pThis->IsRunning())
	{
		int nSpeed = pThis->GetBackupSpeed();
		if (nSpeed <= 0) nSpeed = 1;
		int nSleeTime = 1000 / nSpeed;
		if (!pThis->DrawFrameImage()){
			Sleep(nSleeTime);
		}
		else{
			Sleep(1);
		}
	}
	return 0;
}

BOOL CBackupView::IsRunning()
{
	return m_bRunning;
}

void CBackupView::Quit()
{
	m_bRunning = FALSE;
	if (m_hThread)
	{
		::TerminateThread(m_hThread, 0);
		::CloseHandle(m_hThread);
		m_hThread = NULL;
	}
}

int CBackupView::GetBackupSpeed()
{
	return m_nSpeed;
}

void CBackupView::SetBackupSpeed(int nSpeed)
{
	if (m_nSpeed != nSpeed)
	{
		m_nSpeed = nSpeed;
	}
}

void CBackupView::SetCaptureStatus(BOOL bIsCap)
{
	if (m_bIsCapture != bIsCap)
	{
		m_bIsCapture = bIsCap;
	}
}

BOOL CBackupView::IsCapture()
{
	return m_bIsCapture;
}

void CBackupView::OnTimer(UINT nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default
	
	CWnd::OnTimer(nIDEvent);
}

BOOL CBackupView::DrawFrameImage()
{
	BOOL				bRet = FALSE;
	CUniqueObject *		pObject = CUniqueObject::GetObject();
	CPostMessage *		pPostMessage = CPostMessage::GetObject();
	
	//如果现在还是抓捕状态那么就一直播
	int nPos = GetBackupPos();
	pObject->Lock();
	int nCount = pObject->GetCaptureImageCount();
	if (nPos != nCount)
	{
		HBITMAP hBmp = pObject->FindCaptureImage(nPos);
		if (hBmp)
		{
			if (IsWindow(m_hWnd)) Draw(hBmp, m_hWnd);
			nPos = nPos + 1;
			pPostMessage->PostMessage(UM_SET_SPEED_POS, nPos, 0);
		}
	}
	//如果不是抓捕状态了，并且帧播放到最后一个
	if(!IsCapture() && (nPos == nCount))
	{
		bRet = TRUE;
	}
	pObject->UnLock();

	return bRet;
}

void CBackupView::OnLButtonUp(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	CUniqueObject * pObject = CUniqueObject::GetObject();
	CPostMessage * pPostMessage = CPostMessage::GetObject();
	pObject->Lock();
	do 
	{
		int nImageCount = pObject->GetCaptureImageCount();
		if (0 == nImageCount) break;
		int nCount = 0;
		if (m_nCardsNum == 0){
			nCount = m_nPlayer * 3;
		}
		else if (m_nCardsNum == 1){
			nCount = 4;
		}
		else if (m_nCardsNum == 2){
			nCount = 54;
		}
		std::map<int,HBITMAP> * pSelectedList = pObject->GetSelectedList();
		int nSelectedCount = pSelectedList->size();
		if (nCount == nSelectedCount) break;//一开始就已经满了就不用添加了
		int nPos = GetBackupPos();
		std::map<int,HBITMAP> * pCaptureImage = pObject->GetCaptureImageMap();
		std::map<int,HBITMAP>::iterator it = pCaptureImage->find(nPos - 1);
		if (it == pCaptureImage->end()) break;
		pSelectedList->insert(std::make_pair(it->first, it->second));
		nSelectedCount = pSelectedList->size();
		pPostMessage->PostMessage(UM_CAPTURE_NUMBER, nSelectedCount, 0);
		{
			CDC *	pDC = GetDC();
			CPen	pen;
			CRect	rcClient;
			
			GetClientRect(rcClient);
			pen.CreatePen(PS_SOLID, 5, RGB(255,0,0));
			CPen * pOldPen = pDC->SelectObject(&pen);
			pDC->MoveTo(rcClient.left, rcClient.top);
			pDC->LineTo(rcClient.left, rcClient.bottom);
			pDC->LineTo(rcClient.right, rcClient.bottom);
			pDC->LineTo(rcClient.right, rcClient.top);
			pDC->LineTo(rcClient.left, rcClient.top);
			pDC->SelectObject(pOldPen);
			pen.DeleteObject();
			ReleaseDC(pDC);
		}
		if (nCount == nSelectedCount){//添加完后满了就需要停止抓图
			pPostMessage->PostMessage(UM_STOP_CAPTURE, 1, 0);
			break;
		}
	} while (0);
	
	pObject->UnLock();

	CWnd::OnLButtonUp(nFlags, point);
}

void CBackupView::SetPlayer(int nPlayer)
{
	m_nPlayer = nPlayer;
}

void CBackupView::SetCardsNum(int nCardsNum)
{
	m_nCardsNum = nCardsNum;
}

int CBackupView::GetBackupPos()
{
	return m_nBackupPos;
}

void CBackupView::SetBackupPos(int nPos)
{
	m_nBackupPos = nPos;
}
