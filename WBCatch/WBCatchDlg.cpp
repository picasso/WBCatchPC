// WBCatchDlg.cpp : implementation file
//

#include "stdafx.h"
#include "WBCatch.h"
#include "common.h"
#include "WBCatchDlg.h"
#include "YuvFormat.h"
#include "DlgProrertySet.h"
#include "DlgResolution.h"
#include "DlgBrowse.h"
#include "UniqueObject.h"
#include "SQLiteHelper.h"
#include "PostMessage.h"
#include "CheckKey.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define TIMER_CAPTURE	6377
static UINT uIndicators[] = {IDS_STR_LEFT, IDS_STR_CAPTURE_TIME, IDS_STR_SELECT_IMAGE};

/////////////////////////////////////////////////////////////////////////////
// CWBCatchDlg dialog

CWBCatchDlg::CWBCatchDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CWBCatchDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CWBCatchDlg)
	m_nCamServerID = 0;
	m_pBackupView = NULL;
	m_pPlayView = NULL;
	m_bIsCapture = FALSE;
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CWBCatchDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CWBCatchDlg)
	DDX_Control(pDX, IDC_STATIC_PLAY_POS_TIME, m_staPlayPosTime);
	DDX_Control(pDX, IDC_STATIC_CARDS_NUM, m_staCardsNum);
	DDX_Control(pDX, IDC_COMB_CARDS_NUM, m_combCardsNum);
	DDX_Control(pDX, IDC_STATIC_VIDEO, m_staPlayer);
	DDX_Control(pDX, IDC_STATIC_PLAYERS, m_staPlayers);
	DDX_Control(pDX, IDC_STATIC_PLAY_SPEED, m_staPlaySpeed);
	DDX_Control(pDX, IDC_STATIC_PLAY_POSITION, m_staPlayPos);
	DDX_Control(pDX, IDC_STATIC_MAX_CAPTURE, m_staMaxCap);
	DDX_Control(pDX, IDC_STATIC_GROUP_RIGHT, m_btnGroupRight);
	DDX_Control(pDX, IDC_STATIC_GROUP_LEFT, m_btnGroupLeft);
	DDX_Control(pDX, IDC_STATIC_CAMERA, m_staCamera);
	DDX_Control(pDX, IDC_STATIC_BACKPLAY, m_staBacupPlay);
	DDX_Control(pDX, IDC_SPIN_MAX_CAPTURE, m_spinMaxCap);
	DDX_Control(pDX, IDC_SLIDER_PLAY_SPEED, m_sliPlaySpeed);
	DDX_Control(pDX, IDC_SLIDER_PLAY_POSITION, m_sliPlayPos);
	DDX_Control(pDX, IDC_EDT_MAX_CAPTURE, m_edtMaxCap);
	DDX_Control(pDX, IDC_COMB_PLAYERS, m_combPlayers);
	DDX_Control(pDX, IDC_BTN_STOP_CAPTURE, m_btnStopCap);
	DDX_Control(pDX, IDC_BTN_START_CAPTURE, m_btnStartCap);
	DDX_Control(pDX, IDC_BTN_RESOLUTION, m_btnResolution);
	DDX_Control(pDX, IDC_BTN_PROPERTY, m_btnProperty);
	DDX_Control(pDX, IDC_BTN_BROWSE, m_btnBrowse);
	DDX_Control(pDX, IDC_COMB_DEVICE_LIST, m_combDeviceList);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CWBCatchDlg, CDialog)
	//{{AFX_MSG_MAP(CWBCatchDlg)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BTN_START_CAPTURE, OnBtnStartCapture)
	ON_BN_CLICKED(IDC_BTN_STOP_CAPTURE, OnBtnStopCapture)
	ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_SLIDER_PLAY_POSITION, OnReleasedcaptureSliderPlayPosition)
	ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_SLIDER_PLAY_SPEED, OnReleasedcaptureSliderPlaySpeed)
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_BTN_PROPERTY, OnBtnProperty)
	ON_BN_CLICKED(IDC_BTN_RESOLUTION, OnBtnResolution)
	ON_BN_CLICKED(IDC_BTN_BROWSE, OnBtnBrowse)
	ON_WM_ERASEBKGND()
	ON_WM_CLOSE()
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_MAX_CAPTURE, OnDeltaposSpinMaxCapture)
	ON_CBN_SELCHANGE(IDC_COMB_DEVICE_LIST, OnSelchangeCombDeviceList)
	ON_CBN_SELCHANGE(IDC_COMB_PLAYERS, OnSelchangeCombPlayers)
	ON_CBN_SELCHANGE(IDC_COMB_CARDS_NUM, OnSelchangeCombCardsNum)
	ON_MESSAGE(UM_SET_SPEED_TOTAL, OnSetSpeedTotal)
	ON_MESSAGE(UM_SET_SPEED_POS, OnSetSpeedPos)
	ON_MESSAGE(UM_STOP_CAPTURE, OnStopCapture)
	ON_WM_CREATE()
	ON_MESSAGE(UM_CAPTURE_NUMBER, OnSetCaptureNumber)
	ON_WM_GETMINMAXINFO()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CWBCatchDlg message handlers

BOOL CWBCatchDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here
	m_spinMaxCap.SetRange(40, 100);
	//回放速度
	m_sliPlaySpeed.SetRange(1, 40);
	SetBackupTotalFrames(0);
	//玩家人数
	CString strFormat;
	for (int nIndex = 2; nIndex <= 12; nIndex++)
	{
		strFormat.Format(_T("%d"), nIndex);
		m_combPlayers.AddString(strFormat);
	}
	//牌类型
	m_combCardsNum.AddString(_T("金花"));
	m_combCardsNum.AddString(_T("牌九"));
	m_combCardsNum.AddString(_T("跑得快"));

	m_pPlayView = new CPlayView();
	if (m_pPlayView)
	{
		m_pPlayView->CreateWnd( 0, WS_CHILD | WS_VISIBLE, CRect(0, 0, 1, 1), this, 0 );
		InitDeviceList();
		m_pPlayView->InitDevice();
		//获取当前选中的设备并播放
		int nCurSel = m_combDeviceList.GetCurSel();
		if (nCurSel != CB_ERR)
		{
			int nSession = m_combDeviceList.GetItemData(nCurSel);
			if (m_pPlayView)
			{
				m_pPlayView->SetCameraChannel(nSession);
				m_pPlayView->Player();
			}
		}
	}

	m_pBackupView = new CBackupView();
	if (m_pBackupView)
	{
		m_pBackupView->CreateWnd( 0, WS_CHILD | WS_VISIBLE, CRect(0, 0, 1, 1), this, 0 );
	}

	//读取配置文件数据
	CSQLiteHelper * pSQLObject = CSQLiteHelper::GetObject();
	pSQLObject->ExecuteSQL(_T("select player, cards_num, max_cap_time, play_speed from tb_config;"), SQLiteConfig, this);

	CRect rcWork;
	SystemParametersInfo(SPI_GETWORKAREA, 0, &rcWork, 0) ;
	rcWork.left = (rcWork.Width() - 800) / 2;
	rcWork.right = rcWork.left + 800;
	rcWork.top = (rcWork.Height() - 600) / 2;
	rcWork.bottom = rcWork.top + 600;
	MoveWindow(rcWork);

	//注册窗口消息
	CPostMessage * pPostMessage = CPostMessage::GetObject();
	pPostMessage->RegisterMessage(m_hWnd, UM_SET_SPEED_TOTAL);
	pPostMessage->RegisterMessage(m_hWnd, UM_SET_SPEED_POS);
	pPostMessage->RegisterMessage(m_hWnd, UM_STOP_CAPTURE);
	pPostMessage->RegisterMessage(m_hWnd, UM_CAPTURE_NUMBER);
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CWBCatchDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CPaintDC	dc(this);
		CRect		rcClient;

		GetClientRect(&rcClient);
		dc.FillSolidRect(rcClient, GetSysColor(COLOR_3DFACE));//用对话框背景色填充
	}
}

BOOL CWBCatchDlg::OnEraseBkgnd( CDC* pDC )
{
	return TRUE;
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CWBCatchDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CWBCatchDlg::OnTimer(UINT nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default
	if (TIMER_CAPTURE == nIDEvent)
	{
		if (m_pPlayView)
		{
			time_t	tStart = m_pPlayView->GetStartCapTime();
			time_t	tTime;
			CString	strText;

			time(&tTime);
			time_t t = tTime - tStart;
			strText.Format(_T("已经抓捕的时间：%d"), t);
			SetStatusBarText(1, strText);
		}
	}
	CDialog::OnTimer(nIDEvent);
}

//初始化设备列表
void CWBCatchDlg::InitDeviceList()
{
	CCameraMngr* pCameraManager = CCameraMngr::getInstance();
	list<CAM_GROUP_INFO> groupInfos;
	list<CAM_INFO> camInfos;

	pCameraManager->RetrieveCamGroupListFromServer(m_nCamServerID, &groupInfos);
	list<CAM_GROUP_INFO>::const_iterator groupIter;
	for (groupIter = groupInfos.begin(); groupIter != groupInfos.end(); groupIter++)
	{
		pCameraManager->RetrieveCamListFromServer(m_nCamServerID, groupIter->nCamGroupID, &camInfos);
	}
	
	list<CAM_INFO>& cameras = pCameraManager->GetLiveCameras();
	list<CAM_INFO>::const_iterator iter;
	for (iter = cameras.begin(); iter != cameras.end(); iter++)
	{
		if (iter->state == MP_CAM_StateOnline)
		{
			int nIndex = m_combDeviceList.AddString(iter->szLocation);
			m_combDeviceList.SetItemData(nIndex, iter->nSession);
		}
	}
	m_combDeviceList.SetCurSel(0);
	pCameraManager->ReleaseLiveCameras();
	pCameraManager->FreeCamList(camInfos);
	pCameraManager->FreeCamGroupList(groupInfos);
}

////设置服务器ID
void CWBCatchDlg::SetCamServerID(int nCamServerID)
{
	m_nCamServerID = nCamServerID;
}

void CWBCatchDlg::OnBtnStartCapture() 
{
	// TODO: Add your control notification handler code here
	if (!m_bIsCapture)
	{
		CUniqueObject * pObject = CUniqueObject::GetObject();
		pObject->Lock();
		m_bIsCapture = TRUE;
		SetBackupTotalFrames(0);
		SetBackupPos(0);
		m_btnStartCap.EnableWindow(!m_bIsCapture);
		m_btnStopCap.EnableWindow(m_bIsCapture);
		if (m_pPlayView)
		{
			m_pPlayView->SetCaptureStatus(m_bIsCapture);
		}
		if (m_pBackupView)
		{
			m_pBackupView->SetCaptureStatus(m_bIsCapture);
		}
		//开启计时器
		KillTimer(TIMER_CAPTURE);
		SetTimer(TIMER_CAPTURE, 1000, 0);

		//设置状态文本
		SetStatusBarText(1, _T("已经抓捕的时间：0"));
		SetStatusBarText(2, _T("选中图片数：0"));
		
		pObject->ClearCaptureImageMap();
		pObject->ClearSelected();
		pObject->UnLock();
	}
}

void CWBCatchDlg::OnBtnStopCapture() 
{
	// TODO: Add your control notification handler code here
	if (m_bIsCapture)
	{
		m_bIsCapture = FALSE;
		if (m_pPlayView)
		{
			m_pPlayView->SetCaptureStatus(m_bIsCapture);
		}
		if (m_pBackupView)
		{
			m_pBackupView->SetCaptureStatus(m_bIsCapture);
		}
		//杀死计时器
		KillTimer(TIMER_CAPTURE);
		SetStatusBarText(1, _T("已经抓捕的时间：0"));

		m_btnStartCap.EnableWindow(!m_bIsCapture);
		m_btnStopCap.EnableWindow(m_bIsCapture);
	}
}

void CWBCatchDlg::OnReleasedcaptureSliderPlayPosition(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	OnChangeBackupPos();
	*pResult = 0;
}

void CWBCatchDlg::OnReleasedcaptureSliderPlaySpeed(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	OnChangeBackupSpeed();
	*pResult = 0;
}

//回放速度发生改变
void CWBCatchDlg::OnChangeBackupSpeed()
{
	SetBackupSpeedTip();
}

//回放帧位置发生改变
void CWBCatchDlg::OnChangeBackupPos()
{
	SetBackupPosTip();
}

//设置回放总帧数(该值是变动的)
void CWBCatchDlg::SetBackupTotalFrames(int nFrames)
{
	if (IsWindow(m_sliPlayPos.GetSafeHwnd()))
	{
		m_sliPlayPos.SetRange(0, nFrames);
		SetBackupPosTip();
	}
}

//设置回放帧位置提示
void CWBCatchDlg::SetBackupPosTip()
{
	if (IsWindow(m_sliPlayPos.GetSafeHwnd()))
	{
		CString strFormat;
		int nPos = m_sliPlayPos.GetPos();
		int nMax = m_sliPlayPos.GetRangeMax();
		strFormat.Format(_T("%d/%d"), nPos, nMax);
		m_staPlayPosTime.SetWindowText(strFormat);
		if (m_pBackupView)
		{
			m_pBackupView->SetBackupPos(nPos);
		}
	}
}

//设置回放帧位置
void CWBCatchDlg::SetBackupPos(int nPos)
{
	if (IsWindow(m_sliPlayPos.GetSafeHwnd()))
	{
		m_sliPlayPos.SetPos(nPos);
		SetBackupPosTip();
	}
}

//获取回放帧位置
int CWBCatchDlg::GetBackupPos()
{
	int	nPos = 0;
	if (IsWindow(m_sliPlayPos.GetSafeHwnd()))
	{
		nPos = m_sliPlayPos.GetPos();
	}
	return nPos;
}

//设置播放速度
void CWBCatchDlg::SetBackupSpeed(int nPos)
{
	m_sliPlaySpeed.SetPos(nPos);
	SetBackupSpeedTip();
}

//设置播放速度提示
void CWBCatchDlg::SetBackupSpeedTip()
{
	CString strFormat;
	
	int nPos = m_sliPlaySpeed.GetPos();
	if (m_pBackupView)
	{
		m_pBackupView->SetBackupSpeed(nPos);
	}
	strFormat.Format(_T("播放速度：%d帧/秒"), nPos);
	m_staPlaySpeed.SetWindowText(strFormat);
}

//初始化开始数据
void CWBCatchDlg::Refurbish()
{
	//初始化坐标
	const int   GAP = 10;//窗口四周的间距
	int			nBottomFrameHeight = 0;//底部框架的高度
	CRect		rcItem;
	CRect		rcClient;
	CSize		sTextSize;
	CString		strText;
	int			nWidth = 0;
	int			nHeight = 0;
	int			nStatusBarHeight = 25;

	do 
	{
		GetClientRect(&rcClient);
		if (!IsWindow(m_btnGroupLeft.GetSafeHwnd())) break;
		m_btnGroupLeft.GetClientRect(&rcItem);
		nBottomFrameHeight = rcItem.Height() + nStatusBarHeight;
		
		//播放提示文本坐标
		if (!IsWindow(m_staPlayer.GetSafeHwnd())) break;
		m_staPlayer.GetWindowText(strText);
		sTextSize = GetTextExtent(strText);
		rcItem.left = rcClient.Width()/4 - sTextSize.cx/2;
		rcItem.right = rcItem.left + sTextSize.cx;
		rcItem.top = GAP;
		rcItem.bottom = rcItem.top + sTextSize.cy;
		m_staPlayer.MoveWindow(rcItem);
		//回放提示文本
		if (!IsWindow(m_staBacupPlay.GetSafeHwnd())) break;
		m_staBacupPlay.GetWindowText(strText);
		sTextSize = GetTextExtent(strText);
		rcItem.left = rcClient.Width()/2 + rcClient.Width()/4 - sTextSize.cx/2;
		rcItem.right = rcItem.left + sTextSize.cx;
		m_staBacupPlay.MoveWindow(rcItem);
		
		//播放视频框架的坐标
		if (m_pPlayView)
		{
			rcItem.left = GAP;
			rcItem.right = rcClient.Width()/2 - GAP;
			rcItem.top = rcItem.bottom;
			rcItem.bottom = rcClient.Height() - GAP * 2 - nBottomFrameHeight;
			m_pPlayView->MoveWindow(rcItem);
		}
		
		//回放框架坐标
		if (m_pBackupView)
		{
			rcItem.left = rcItem.right + GAP;
			rcItem.right = rcClient.Width() - GAP;
			m_pBackupView->MoveWindow(rcItem);
		}
		
		//左边设置框架坐标
		if (!IsWindow(m_btnGroupLeft.GetSafeHwnd())) break;
		rcItem.left = GAP;
		rcItem.right = rcClient.Width()/2 - GAP;
		rcItem.top = rcClient.Height() - nBottomFrameHeight - GAP;
		rcItem.bottom = rcItem.top + nBottomFrameHeight - nStatusBarHeight;
		m_btnGroupLeft.MoveWindow(rcItem);
		//右边设置框架坐标
		if (!IsWindow(m_btnGroupRight.GetSafeHwnd())) break;
		rcItem.left = rcItem.right + GAP;
		rcItem.right = rcClient.Width() - GAP;
		m_btnGroupRight.MoveWindow(rcItem);

		//底部框架的按钮坐标
		//开始抓捕
		if (!IsWindow(m_btnStartCap.GetSafeHwnd())) break;
		m_btnStartCap.GetClientRect(&rcItem);
		nWidth = rcItem.Width();
		nHeight = rcItem.Height();
		rcItem.left = 2* GAP;
		rcItem.right = rcItem.left + nWidth;
		rcItem.top = rcClient.Height() - nBottomFrameHeight + GAP;
		rcItem.bottom = rcItem.top + nHeight;
		m_btnStartCap.MoveWindow(rcItem);
		//停止抓捕
		if (!IsWindow(m_btnStopCap.GetSafeHwnd())) break;
		rcItem.left = rcItem.right + GAP;
		rcItem.right = rcItem.left + nWidth;
		m_btnStopCap.MoveWindow(rcItem);
		//属性设置
		if (!IsWindow(m_btnProperty.GetSafeHwnd())) break;
		rcItem.left = 2* GAP;
		rcItem.right = rcItem.left + nWidth;
		rcItem.top = rcItem.bottom + GAP;
		rcItem.bottom = rcItem.top + nHeight;
		m_btnProperty.MoveWindow(rcItem);
		//分辨率
		if (!IsWindow(m_btnResolution.GetSafeHwnd())) break;
		rcItem.left = rcItem.right + GAP;
		rcItem.right = rcItem.left + nWidth;
		m_btnResolution.MoveWindow(rcItem);
		//浏览图片
		if (!IsWindow(m_btnBrowse.GetSafeHwnd())) break;
		rcItem.left = 2* GAP;
		rcItem.right = rcItem.left + 2* nWidth + GAP;
		rcItem.top = rcItem.bottom + GAP;
		rcItem.bottom = rcItem.top + nHeight;
		m_btnBrowse.MoveWindow(rcItem);
		//摄像机提示
		m_staCamera.GetWindowText(strText);
		sTextSize = GetTextExtent(strText);
		rcItem.left = rcItem.right + 30;
		rcItem.right = rcItem.left + sTextSize.cx;
		rcItem.top = rcClient.Height() - nBottomFrameHeight + GAP;
		rcItem.bottom = rcItem.top + sTextSize.cy;
		m_staCamera.MoveWindow(rcItem);
		//牌类型
		if (!IsWindow(m_staCardsNum.GetSafeHwnd())) break;
		m_staCardsNum.GetWindowText(strText);
		sTextSize = GetTextExtent(strText);
		rcItem.right = rcItem.left + sTextSize.cx;
		rcItem.top = rcItem.bottom + GAP;
		rcItem.bottom = rcItem.top + sTextSize.cy;
		m_staCardsNum.MoveWindow(rcItem);
		//玩家人数提示
		m_staPlayers.GetWindowText(strText);
		sTextSize = GetTextExtent(strText);
		rcItem.right = rcItem.left + sTextSize.cx;
		rcItem.top = rcItem.bottom + GAP;
		rcItem.bottom = rcItem.top + sTextSize.cy;
		m_staPlayers.MoveWindow(rcItem);
		
		//最大抓捕时间提示
		m_staMaxCap.GetWindowText(strText);
		sTextSize = GetTextExtent(strText);
		rcItem.right = rcItem.left + sTextSize.cx;
		rcItem.top = rcItem.bottom + GAP;
		rcItem.bottom = rcItem.top + sTextSize.cy;
		m_staMaxCap.MoveWindow(rcItem);
		//摄像机列表
		CRect	rc;
		int		nLeft = rcItem.left;
		m_combDeviceList.GetClientRect(&rc);
		nWidth = rc.Width();
		nHeight = rc.Height();
		m_staCamera.GetWindowText(strText);
		sTextSize = GetTextExtent(strText);
		rcItem.left = nLeft + sTextSize.cx;
		rcItem.right = rcItem.left + nWidth;
		rcItem.top = rcClient.Height() - nBottomFrameHeight + 7;
		rcItem.bottom = rcItem.top + nHeight;
		m_combDeviceList.MoveWindow(rcItem);
		//牌类型
		if (!IsWindow(m_combCardsNum.GetSafeHwnd())) break;
		m_staCardsNum.GetWindowText(strText);
		sTextSize = GetTextExtent(strText);
		rcItem.left = nLeft + sTextSize.cx;
		rcItem.top = rcItem.bottom + 5;
		rcItem.bottom = rcItem.top + nHeight;
		m_combCardsNum.MoveWindow(rcItem);
		//玩家人数
		m_staPlayers.GetWindowText(strText);
		sTextSize = GetTextExtent(strText);
		rcItem.left = nLeft + sTextSize.cx;
		rcItem.top = rcItem.bottom + 5;
		rcItem.bottom = rcItem.top + nHeight;
		m_combPlayers.MoveWindow(rcItem);

		//最大抓捕时间
		m_staMaxCap.GetWindowText(strText);
		sTextSize = GetTextExtent(strText);
		rcItem.left = nLeft + sTextSize.cx - 10;
		rcItem.top = rcItem.bottom + 5;
		rcItem.bottom = rcItem.top + nHeight;
		m_edtMaxCap.MoveWindow(rcItem);
		m_spinMaxCap.SetBuddy(&m_edtMaxCap);
		
		//右边框架坐标
		m_btnGroupRight.GetWindowRect(rc);
		ScreenToClient(&rc);
		//播放速度提示
		m_staPlaySpeed.GetWindowText(strText);
		sTextSize = GetTextExtent(strText);
		rcItem.left = rc.left + GAP;
		rcItem.right = rcItem.left + 200;
		rcItem.top = rc.top + 20;
		rcItem.bottom = rcItem.top + sTextSize.cy;
		m_staPlaySpeed.MoveWindow(rcItem);
		//播放速度
		CRect	rcSli;
		m_sliPlaySpeed.GetClientRect(rcSli);
		rcItem.right = rc.right - GAP;
		rcItem.top = rcItem.bottom + GAP;
		rcItem.bottom = rcItem.top + rcSli.Height();
		m_sliPlaySpeed.MoveWindow(rcItem);
		//播放位置提示
		m_staPlayPos.GetWindowText(strText);
		sTextSize = GetTextExtent(strText);
		rcItem.left = rc.left + GAP;
		rcItem.right = rcItem.left + sTextSize.cx;
		rcItem.top = rcItem.bottom + 5;
		rcItem.bottom = rcItem.top + sTextSize.cy;
		m_staPlayPos.MoveWindow(rcItem);
		CRect rcTime = rcItem;
		rcTime.left = rcTime.right - GAP;
		rcTime.right = rcTime.left + 200;
		m_staPlayPosTime.MoveWindow(rcTime);
		//播放位置
		m_sliPlayPos.GetClientRect(rcSli);
		rcItem.right = rc.right - GAP;
		rcItem.top = rcItem.bottom + 5;
		rcItem.bottom = rcItem.top + rcSli.Height();
		m_sliPlayPos.MoveWindow(rcItem);
		//状态栏
		if (IsWindow(m_wndStatusBar.GetSafeHwnd()))
		{
			rcItem.left = 0;
			rcItem.right = rcClient.right;
			rcItem.top = rcClient.bottom - nStatusBarHeight;
			rcItem.bottom = rcClient.bottom;
			m_wndStatusBar.MoveWindow(rcItem);
		}
	} while (0);
}

//获取文本区大小
CSize CWBCatchDlg::GetTextExtent(CString str)
{
	CSize	size;
	CDC *   pDC = GetDC();
	
	size = pDC->GetTextExtent(str);
	ReleaseDC(pDC);
	return size;
}

void CWBCatchDlg::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
	Refurbish();
}

void CWBCatchDlg::OnBtnProperty() 
{
	// TODO: Add your control notification handler code here
	CDlgProrertySet	dlg(m_nCamServerID);
	dlg.DoModal();
}

void CWBCatchDlg::OnBtnResolution() 
{
	// TODO: Add your control notification handler code here
	CDlgResolution dlg(m_nCamServerID);
	dlg.DoModal();
}

void CWBCatchDlg::OnBtnBrowse() 
{
	// TODO: Add your control notification handler code here
	CString	str;
	m_combPlayers.GetWindowText(str);
	int nPlayers = _ttoi(str);
    int nCardsNum = m_combCardsNum.GetCurSel();

	CUniqueObject * pObject = CUniqueObject::GetObject();
	pObject->Lock();
	int nImageCount = pObject->GetSelectedList()->size();
	pObject->UnLock();
	if (nCardsNum == 0)
	{
		int nCount = nPlayers * 3;
		if (nCount == nImageCount)
		{
			CDlgBrowse dlg(nPlayers, nCardsNum, 0);
			dlg.DoModal();
		}
		else
		{
			CString	str;
			str.Format(_T("请选择%d张牌"), nCount);
			AfxMessageBox(str);
		}
	}
	else if(nCardsNum == 1)
	{
		if (nImageCount == 4)
		{
			CDlgBrowse dlg(nPlayers, nCardsNum, 0);
			dlg.DoModal();
		}
		else
		{
			AfxMessageBox(_T("请选择4张牌"));
		}
	}
	else if (nCardsNum = 2)
	{
		if (nImageCount == 54)
		{
			CDlgBrowse dlg(nPlayers, nCardsNum, 0);
			dlg.DoModal();
		}
		else
		{
			AfxMessageBox(_T("请选择54张牌"));
		}
	}
}

void CWBCatchDlg::Quit()
{
	CUniqueObject * pObject = CUniqueObject::GetObject();
	pObject->Lock();
	
	InsertConfig();

	OnBtnStopCapture();

	if (m_pPlayView)
	{
		m_pPlayView->Quit();
		m_pPlayView->DestroyWindow();
		delete m_pPlayView;
		m_pPlayView = NULL;
	}

	if (m_pBackupView)
	{
		m_pBackupView->Quit();
		m_pBackupView->DestroyWindow();
		delete m_pBackupView;
		m_pBackupView = NULL;
	}
	
	CCameraMngr* pCameraManager = CCameraMngr::getInstance();
	pCameraManager->DisconnectServer(m_nCamServerID);

	pObject->UnLock();
}


void CWBCatchDlg::OnClose() 
{
	// TODO: Add your message handler code here and/or call default
	Quit();
	CDialog::OnClose();
}

void CWBCatchDlg::OnDeltaposSpinMaxCapture(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	// TODO: Add your control notification handler code here
	int nMin = 0;
	int nMax = 0;
	int nPos = 0;
	int	nMaxCap = 0;

	m_spinMaxCap.GetRange(nMin, nMax);
	nPos = pNMUpDown->iPos + pNMUpDown->iDelta;
	if (pNMUpDown->iDelta > 0){
		if (nPos > nMax)
			nMaxCap = nMax;
		else
			nMaxCap = nPos;
	}
	else{
		if (nPos < nMin)
			nMaxCap = nMin;
		else
			nMaxCap = nPos;
	}
	if (m_pPlayView){
		m_pPlayView->SetMaxCapTime(nMaxCap);
	}

	*pResult = 0;
}

void CWBCatchDlg::OnSelchangeCombDeviceList() 
{
	// TODO: Add your control notification handler code here
	int nCurSel = m_combDeviceList.GetCurSel();
	if (nCurSel != CB_ERR)
	{
		int nSession = m_combDeviceList.GetItemData(nCurSel);
		if (m_pPlayView)
		{
			m_pPlayView->Stop();
			m_pPlayView->SetCameraChannel(nSession);
			m_pPlayView->Player();
		}
	}
}

void CWBCatchDlg::OnSelchangeCombPlayers() 
{
	// TODO: Add your control notification handler code here
	CString	str;
	m_combPlayers.GetWindowText(str);
	int nPlayers = _ttoi(str);
	if (m_pBackupView)
	{
		m_pBackupView->SetPlayer(nPlayers);
	}
}

void CWBCatchDlg::OnSelchangeCombCardsNum() 
{
	// TODO: Add your control notification handler code here
	CString	str;
    int nCardsNum = m_combCardsNum.GetCurSel();
	if (m_pBackupView)
	{
		m_pBackupView->SetCardsNum(nCardsNum);
	}
}

////插入配置信息
void CWBCatchDlg::InsertConfig()
{
	CSQLiteHelper *	pSqlHelper = CSQLiteHelper::GetObject();
	CString	strSQL;
	CString	str;
	m_combPlayers.GetWindowText(str);
	int nPlayers = _ttoi(str);
    int nCardsNum = m_combCardsNum.GetCurSel();
	int nMaxCap = m_spinMaxCap.GetPos();
	int nSpeed = m_sliPlaySpeed.GetPos();
	strSQL.Format(_T("replace into tb_config (id, player, cards_num, max_cap_time, play_speed) values(%d, %d, %d, %d, %d);"), 0, nPlayers, nCardsNum, nMaxCap, nSpeed);
	std::string szUTF8 = FormatUTF8(strSQL);
	pSqlHelper->ExecuteSQL(szUTF8.c_str(), NULL, NULL);
}

int CWBCatchDlg::SQLiteConfig(void* pParam, int nCount, char** pValue, char** pName)
{
	CWBCatchDlg * pThis = static_cast<CWBCatchDlg *>(pParam);
	std::map<std::string, std::string>	UserInfo;
	for (int i = 0; i < nCount; i++)
	{
		UserInfo.insert( std::make_pair(pName[i], pValue[i]) );
	}
	std::map<std::string, std::string>::iterator itFind = UserInfo.find("player");
	if (itFind != UserInfo.end())
	{
		pThis->SetPlayer(atoi(itFind->second.c_str()));
	}
	
	itFind = UserInfo.find("cards_num");
	if (itFind != UserInfo.end())
	{
		pThis->SetCardsNum(atoi(itFind->second.c_str()));
	}
	
	itFind = UserInfo.find("max_cap_time");
	if (itFind != UserInfo.end())
	{
		pThis->SetMaxCapture(atoi(itFind->second.c_str()));
	}
	
	itFind = UserInfo.find("play_speed");
	if (itFind != UserInfo.end())
	{
		pThis->SetBackupSpeed(atoi(itFind->second.c_str()));
	}
	return 0;
}

void CWBCatchDlg::SetMaxCapture(int nPos)
{
	m_spinMaxCap.SetPos(nPos);
	if(m_pPlayView) m_pPlayView->SetMaxCapTime(nPos);
}

void CWBCatchDlg::SetCardsNum(int nCards)
{
	m_combCardsNum.SetCurSel(nCards);
	if(m_pBackupView) m_pBackupView->SetCardsNum(nCards);
}

void CWBCatchDlg::SetPlayer(int nPlayer)
{
	CString	str;
	int nValues = 0;
	int nCount = m_combPlayers.GetCount();
	for(int i = 0; i < nCount; i++)
	{
		m_combPlayers.GetLBText(i, str);
		nValues = _ttoi(str);
		if (nValues == nPlayer)
		{
			m_combPlayers.SetCurSel(i);
			if(m_pBackupView) m_pBackupView->SetPlayer(nValues);
			break;
		}
	}
}

LRESULT CWBCatchDlg::OnSetSpeedTotal(WPARAM wParam, LPARAM lParam)
{
	SetBackupTotalFrames((int)wParam);
	return 0;
}

LRESULT CWBCatchDlg::OnSetSpeedPos(WPARAM wParam, LPARAM lParam)
{
	SetBackupPos((int)wParam);
	return 0;
}

LRESULT CWBCatchDlg::OnStopCapture(WPARAM wParam, LPARAM lParam)
{
	OnBtnStopCapture();
	if (wParam == 1)
	{
		OnBtnBrowse();
	}
	return 0;
}

int CWBCatchDlg::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CDialog::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	// TODO: Add your specialized creation code here
	if (!m_wndStatusBar.Create(this) || !m_wndStatusBar.SetIndicators(uIndicators, sizeof(uIndicators)/sizeof(UINT)))
	{
		return -1;
	}
	m_wndStatusBar.SetPaneInfo(0, IDS_STR_LEFT, SBPS_DISABLED, 10);
	SetStatusBarText(1, _T("已经抓捕的时间：0"));
	SetStatusBarText(2, _T("选中图片数：0"));
	SuperDog_CheckKey();
	return 0;
}

void CWBCatchDlg::SetStatusBarText(int nIndex, LPCTSTR szText)
{
	if (IsWindow(m_wndStatusBar.GetSafeHwnd()))
	{
		CSize size = GetTextExtent(szText);
		switch(nIndex)
		{
		case 1:
			{
				m_wndStatusBar.SetPaneInfo(nIndex, IDS_STR_CAPTURE_TIME, SBPS_NOBORDERS, size.cx);
				m_wndStatusBar.SetPaneText(nIndex, szText);
			}
			break;
		case 2:
			{
				m_wndStatusBar.SetPaneInfo(nIndex, IDS_STR_SELECT_IMAGE, SBPS_NOBORDERS, size.cx);
				m_wndStatusBar.SetPaneText(nIndex, szText);
			}
			break;
		}
	}
}

LRESULT CWBCatchDlg::OnSetCaptureNumber(WPARAM wParam, LPARAM lParam)
{
	int nCount = (int)wParam;
	CString	str;
	str.Format(_T("选中图片数：%d"), nCount);
	SetStatusBarText(2, str);
	return 0;
}

void CWBCatchDlg::OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI)     
{    
    lpMMI->ptMinTrackSize.x = 800;   
    lpMMI->ptMinTrackSize.y = 600;    
    CDialog::OnGetMinMaxInfo(lpMMI);    
} 