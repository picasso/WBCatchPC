// DlgBrowse.cpp : implementation file
//

#include "stdafx.h"
#include "wbcatch.h"
#include "DlgBrowse.h"
#include "YuvFormat.h"
#include "UniqueObject.h"
#include "PostMessage.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDlgBrowse dialog


CDlgBrowse::CDlgBrowse(int nPlayers, int nCardsNum, int nOrder, CWnd* pParent /*=NULL*/)
	: CDialog(CDlgBrowse::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgBrowse)
		// NOTE: the ClassWizard will add member initialization here
	m_nPlayers = nPlayers;
	m_nCardsNum = nCardsNum;
	m_nOrder = nOrder;
	m_pList = NULL;
	//}}AFX_DATA_INIT
}


void CDlgBrowse::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgBrowse)
	DDX_Control(pDX, IDC_BTN_DOWN, m_btnDown);
	DDX_Control(pDX, IDC_BTN_UP, m_btnUp);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgBrowse, CDialog)
	//{{AFX_MSG_MAP(CDlgBrowse)
	ON_WM_CLOSE()
	ON_WM_MOUSEWHEEL()
	ON_WM_RBUTTONUP()
	ON_WM_LBUTTONUP()
	ON_BN_CLICKED(IDC_BTN_UP, OnBtnUp)
	ON_BN_CLICKED(IDC_BTN_DOWN, OnBtnDown)
	ON_MESSAGE(UM_UP_DOWN, OnUpDown)
	ON_WM_GETMINMAXINFO()
	ON_WM_ERASEBKGND()
	ON_WM_PAINT()
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDlgBrowse message handlers
BOOL CDlgBrowse::OnInitDialog()
{
	CDialog::OnInitDialog();
	CPostMessage * pPostMessage = CPostMessage::GetObject();
	pPostMessage->UnRegisterMessage(UM_UP_DOWN);
	pPostMessage->RegisterMessage(m_hWnd, UM_UP_DOWN);

	CRect rcWork;
	SystemParametersInfo(SPI_GETWORKAREA, 0, &rcWork, 0) ;
	rcWork.left = (rcWork.Width() - 800) / 2;
	rcWork.right = rcWork.left + 800;
	rcWork.top = (rcWork.Height() - 600) / 2;
	rcWork.bottom = rcWork.top + 600;
	MoveWindow(rcWork);
	GetClientRect(rcWork);
	//上一页下一页位置
	CRect	rcButton;
	m_btnUp.GetClientRect(&rcButton);
	int		nWidth = rcButton.Width();
	int		nHeight = rcButton.Height();
	rcButton.left = rcWork.Width()/2 - nWidth - 5;
	rcButton.right = rcButton.left + nWidth;
	rcButton.top = rcWork.Height() - nHeight;
	rcButton.bottom = rcButton.top + nHeight;
	m_btnUp.MoveWindow(rcButton);
	rcButton.left = rcButton.right + 10;
	rcButton.right = rcButton.left + nWidth;
	m_btnDown.MoveWindow(rcButton);

	int nStaticHeight = 0;
	if (m_nCardsNum == 0)//金花才显示玩家1、2、3、4
	{
		CString	strText;
		int nStaticWidth = rcWork.Width()/4;
		nStaticHeight = 20;
		CRect rcStatic(0, 0, 0, 0);
		for (int i = 1; i <= 4; i++)
		{
			CStatic * pCtrl = new CStatic();
			if (pCtrl)
			{
				strText.Format(_T("玩家%d"), i);
				rcStatic.left = rcStatic.right + nStaticWidth/2;
				rcStatic.right = rcStatic.left + nStaticWidth/2;
				rcStatic.top = 0;
				rcStatic.bottom = nStaticHeight;
				pCtrl->Create(strText, WS_CHILD|WS_VISIBLE, rcStatic, this);
				m_TipList.push_back(pCtrl);
			}
		}
	}
	
	CRect	rcList = rcWork;
	rcList.top += nStaticHeight;
	rcList.bottom -= rcButton.Height();
	m_pList = new CListCtrlWnd();
	if (m_pList)
	{
		m_pList->CreateWnd( 0, WS_CHILD | WS_VISIBLE, rcList, this, 0 );
		m_pList->m_nCards = m_nCardsNum;
		CUniqueObject * pObject = CUniqueObject::GetObject();
		pObject->Lock();
		std::map<int,HBITMAP> * pImageMap =  pObject->GetSelectedList();
		int		nCount = pImageMap->size();
		BOOL	bInit = TRUE;

		if (m_nCardsNum == 0)
		{
			//金花顺序
			for (std::map<int,HBITMAP>::iterator it = pImageMap->begin(); it != pImageMap->end(); it++)
			{
				if (bInit)
				{
					BITMAP bmp = {0};
					GetObject(it->second, sizeof(BITMAP), (void*)&bmp);
					m_pList->InitEx(0, nCount, bmp.bmWidth, bmp.bmHeight);
					bInit = FALSE;
				}
				m_pList->AddItemEx(it->second, it->first);
			}
		}
		else if (m_nCardsNum == 1)
		{
			//牌9逆序
			for (std::map<int,HBITMAP>::reverse_iterator it = pImageMap->rbegin(); it != pImageMap->rend(); it++)
			{
				if (bInit)
				{
					BITMAP bmp = {0};
					GetObject(it->second, sizeof(BITMAP), (void*)&bmp);
					m_pList->InitEx(1, nCount, bmp.bmWidth, bmp.bmHeight);
					bInit = FALSE;
				}
				m_pList->AddItemEx(it->second, it->first);
			}
		}
		else if (m_nCardsNum == 2)
		{
			//跑得快
			for (std::map<int,HBITMAP>::iterator it = pImageMap->begin(); it != pImageMap->end(); it++)
			{
				if (bInit)
				{
					BITMAP bmp = {0};
					GetObject(it->second, sizeof(BITMAP), (void*)&bmp);
					m_pList->InitEx(2, nCount, bmp.bmWidth, bmp.bmHeight);
					bInit = FALSE;
				}
				m_pList->AddItemEx(it->second, it->first);
			}
		}
		pObject->UnLock();
	}
	
	return TRUE;
}

void CDlgBrowse::OnClose() 
{
	// TODO: Add your message handler code here and/or call default
	std::list<CStatic *>::iterator it = m_TipList.begin();
	CStatic	* pCtrl = NULL;
	while(it != m_TipList.end())
	{
		pCtrl = static_cast<CStatic *>(*it);
		m_TipList.erase(it);
		pCtrl->DestroyWindow();
		delete pCtrl;
		it = m_TipList.begin();
	}

	if (m_pList)
	{
		m_pList->DestroyWindow();
		delete m_pList;
		m_pList = NULL;
	}

	CDialog::OnClose();
}

BOOL CDlgBrowse::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt) 
{
	// TODO: Add your message handler code here and/or call default
	if (m_pList)
	{
		m_pList->Scroll(zDelta);
	}
	return CDialog::OnMouseWheel(nFlags, zDelta, pt);
}

void CDlgBrowse::OnRButtonUp(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	
	CDialog::OnRButtonUp(nFlags, point);
}

void CDlgBrowse::OnLButtonUp(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	
	CDialog::OnLButtonUp(nFlags, point);
}

void CDlgBrowse::OnBtnUp() 
{
	// TODO: Add your control notification handler code here
	if (m_pList)
	{
		m_pList->OnUpPage();
	}
}

void CDlgBrowse::OnBtnDown() 
{
	// TODO: Add your control notification handler code here
	if (m_pList)
	{
		m_pList->OnDownPage();
	}
}

LRESULT CDlgBrowse::OnUpDown(WPARAM wParam, LPARAM lParam)
{
	m_btnUp.EnableWindow(wParam);
	m_btnDown.EnableWindow(lParam);
	return 0;
}

void CDlgBrowse::OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI)     
{    
    lpMMI->ptMinTrackSize.x = 800;   
    lpMMI->ptMinTrackSize.y = 600;    
    CDialog::OnGetMinMaxInfo(lpMMI);    
} 

BOOL CDlgBrowse::OnEraseBkgnd( CDC* pDC )
{
	return TRUE;
}

void CDlgBrowse::OnPaint() 
{
	CPaintDC	dc(this);
	CRect		rcClient;

	GetClientRect(&rcClient);
	dc.FillSolidRect(rcClient, GetSysColor(COLOR_3DFACE));//用对话框背景色填充
}

void CDlgBrowse::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
	CRect	rcWork;

	GetClientRect(rcWork);
	//上一页下一页位置
	CRect	rcButton;
	int		nWidth = 0;
	int		nHeight = 0;
	
	if (::IsWindow(m_btnUp.GetSafeHwnd()))
	{
		m_btnUp.GetClientRect(&rcButton);
		nWidth = rcButton.Width();
		nHeight = rcButton.Height();
		rcButton.left = rcWork.Width()/2 - nWidth - 5;
		rcButton.right = rcButton.left + nWidth;
		rcButton.top = rcWork.Height() - nHeight;
		rcButton.bottom = rcButton.top + nHeight;
		m_btnUp.MoveWindow(rcButton);
	}
	
	if (::IsWindow(m_btnDown.GetSafeHwnd()))
	{
		rcButton.left = rcButton.right + 10;
		rcButton.right = rcButton.left + nWidth;
		m_btnDown.MoveWindow(rcButton);
	}
	
	CRect rcStatic(0, 0, 0, 0);
	int nStaticHeight = 0;
	int nStaticWidth = rcWork.Width()/4;
	CStatic * pCtrl = NULL;
	for(std::list<CStatic *>::iterator it = m_TipList.begin(); it!= m_TipList.end(); it++)
	{
		nStaticHeight = 20;
		rcStatic.left = rcStatic.right + nStaticWidth/2;
		rcStatic.right = rcStatic.left + nStaticWidth/2;
		rcStatic.top = 0;
		rcStatic.bottom = nStaticHeight;
		pCtrl = (*it);
		if (IsWindow(pCtrl->GetSafeHwnd()))
		{
			pCtrl->MoveWindow(rcStatic, TRUE);
		}
	}
	
	CRect	rcList = rcWork;
	rcList.top += nStaticHeight;
	rcList.bottom -= rcButton.Height();
	if (m_pList)
	{
		if (IsWindow(m_pList->GetSafeHwnd()))
		{
			m_pList->OnSize(rcList);
			m_pList->MoveWindow(rcList, TRUE);
			m_pList->Invalidate();
		}
	}
}
