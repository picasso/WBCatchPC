// UniqueObject.cpp: implementation of the CUniqueObject class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "wbcatch.h"
#include "UniqueObject.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CUniqueObject::CUniqueObject()
{
	::InitializeCriticalSection(&m_cs);
}

CUniqueObject::~CUniqueObject()
{
	ClearCaptureImageMap();
	ClearMemory();
	::DeleteCriticalSection(&m_cs);
}

CUniqueObject * CUniqueObject::GetObject()
{
	static CUniqueObject obj;
	return &obj;
}

void CUniqueObject::Lock()
{
	::EnterCriticalSection(&m_cs);
}

void CUniqueObject::UnLock()
{
	::LeaveCriticalSection(&m_cs);
}

std::map<int,HBITMAP> * CUniqueObject::GetCaptureImageMap()
{
	return &m_mapCaptureImage;
}

void CUniqueObject::ClearCaptureImageMap()
{
	HBITMAP	hBmp = NULL;
	std::map<int,HBITMAP>::iterator it = m_mapCaptureImage.begin();
	while(it != m_mapCaptureImage.end())
	{
		hBmp = it->second;
		if (hBmp)
		{
			::DeleteObject(hBmp);
		}
		m_mapCaptureImage.erase(it);
		it = m_mapCaptureImage.begin();
	}
}

int  CUniqueObject::InsertCaptureImage(HBITMAP hBitmap)
{
	int nCount = m_mapCaptureImage.size();
	m_mapCaptureImage.insert(std::make_pair(nCount, hBitmap));
	nCount = m_mapCaptureImage.size();
	return nCount;
}

int CUniqueObject::GetCaptureImageCount()
{
	return m_mapCaptureImage.size();
}

HBITMAP CUniqueObject::FindCaptureImage(int nIndex)
{
	HBITMAP		hBmp = NULL;
	std::map<int,HBITMAP>::iterator it = m_mapCaptureImage.find(nIndex);
	if (it != m_mapCaptureImage.end())
	{
		hBmp = it->second;
	}
	return hBmp;
}

std::map<int,HBITMAP> * CUniqueObject::GetSelectedList()
{
	return &m_listSelected;
}

void CUniqueObject::ClearSelected()
{
	m_listSelected.clear();
}

//////////////////////////////////////////////////////////////////////////
void CUniqueObject::InsertMemory(LPARAM lParam, RGBIMAGEINFO info)
{
	m_mapMemory.insert(std::make_pair(lParam, info));
}

RGBIMAGEINFO CUniqueObject::FindMemory(LPARAM lParam)
{
	RGBIMAGEINFO	info = {0};
	std::map<LPARAM, RGBIMAGEINFO>::iterator it = m_mapMemory.find(lParam);
	if (it != m_mapMemory.end())
	{
		info = it->second;
	}
	return info;
}

void CUniqueObject::DeleteMemory(LPARAM lParam)
{
	BYTE * pBuffer = NULL;
	std::map<LPARAM, RGBIMAGEINFO>::iterator it = m_mapMemory.find(lParam);
	if (it != m_mapMemory.end())
	{
		pBuffer = it->second.pRGB;
		free(pBuffer);
		pBuffer = NULL;
		m_mapMemory.erase(it);
	}
}

void CUniqueObject::ClearMemory()
{
	BYTE * pBuffer = NULL;
	std::map<LPARAM, RGBIMAGEINFO>::iterator it = m_mapMemory.begin();
	while (it != m_mapMemory.end())
	{
		pBuffer = it->second.pRGB;
		free(pBuffer);
		pBuffer = NULL;
		m_mapMemory.erase(it);
		it = m_mapMemory.begin();
	}
}