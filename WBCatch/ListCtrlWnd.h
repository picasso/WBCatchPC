#ifndef __LISTCTRL_WND_H__
#define __LISTCTRL_WND_H__

#pragma warning(disable:4786)
#include <map>

typedef struct  
{
	HBITMAP hBmp;
	CRect	rcItem;
	int		nIndex;
}UITEMINFO;

class CListCtrlWnd : public CWnd
{
public:
	CListCtrlWnd();
	virtual ~CListCtrlWnd();	
public:
	void         Init(int nColumn, int nImageCount, int nImageWidth, int ImageHeight);//窗口创建成功后第一个调用
	BOOL         CreateWnd( DWORD dwExStyle, DWORD dwStyle, const CRect& rc, CWnd* pParent, UINT uID );
	BOOL         AddItem(HBITMAP hBmp, int nIndex);
	int          GetItemCount();
	void		 Scroll(short zDelta);
private:
	afx_msg void OnPaint();
	afx_msg BOOL OnEraseBkgnd( CDC* pDC );
	afx_msg void OnVScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar );
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	DECLARE_MESSAGE_MAP()
protected:
	HBITMAP		 CopyBitmap(HBITMAP  hSourcehBitmap );
private:
	std::map<int, UITEMINFO>	m_mapImage;
	int							m_nItemColumn;
	int							m_nItemWidth;
	int							m_nItemHeight;
	int							m_nCurrentPos;
	BOOL						m_bHasScroll;
	CFont						m_Font;
//新需求处理
public:
	void InitEx(int nCards, int nImageCount, int nImageWidth, int nImageHeight);
	BOOL AddItemEx(HBITMAP hBmp, int nIndex);
	void OnUpPage();
	void OnDownPage();
	void OnSize(CRect	rcClient);
public:
	UITEMINFO					m_ImageList[3][12];
public:
	int		m_nCards;
	int		m_nTotalPages;
	int		m_nCurrentPage;
};

#endif