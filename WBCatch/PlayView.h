#if !defined(AFX_PLAYVIEW_H__44ECA6D8_8936_463A_A592_6F1FCD7CA52B__INCLUDED_)
#define AFX_PLAYVIEW_H__44ECA6D8_8936_463A_A592_6F1FCD7CA52B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PlayView.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CPlayView window

class CPlayView : public CWnd
{
// Construction
public:
	CPlayView();

// Attributes
public:
	BOOL CreateWnd( DWORD dwExStyle, DWORD dwStyle, const CRect& rc, CWnd* pParent, UINT uID );
// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPlayView)
	//}}AFX_VIRTUAL

// Implementation
public:
	void Quit();
	virtual ~CPlayView();

	// Generated message map functions
protected:
	//{{AFX_MSG(CPlayView)
	afx_msg void OnPaint();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg LRESULT OnDrawFrameImage(WPARAM wParam, LPARAM lParam);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	//视频数据回调
	static void UIEventCallBackHandler(MP_ENG_EVENT event, int nIndex, void *pParam, void *pAppData);
	//检查现在是否在抓捕
	BOOL IsCapture();
	//设置抓捕状态
	void SetCaptureStatus(BOOL bIsCap);
	//获取最大抓捕时间
	int GetMaxCapTime();
	//设置最大抓捕时间
	void SetMaxCapTime(int nTime);
	//获取开始抓捕时间
	time_t GetStartCapTime();
	//初始化播放设备
	void InitDevice();
	//播放
	void Player();
	//停止播放
	void Stop();
	//设置设备通道
	void SetCameraChannel(int nCameraChannel);
private:
	BOOL		m_bIsCapture;
	int			m_nMaxCap;
	time_t		m_tStartCapTime;
	int			m_nCameraChannel;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PLAYVIEW_H__44ECA6D8_8936_463A_A592_6F1FCD7CA52B__INCLUDED_)
