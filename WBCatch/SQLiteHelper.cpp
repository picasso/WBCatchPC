#include "stdafx.h"
#include "SQLiteHelper.h"
#include "common.h"

CSQLiteHelper::CSQLiteHelper()
{
	OpenDB();
}

CSQLiteHelper::~CSQLiteHelper()
{
	CloseDB();
}

//获取单体对象
CSQLiteHelper * CSQLiteHelper::GetObject()
{
	static CSQLiteHelper obj;
	return &obj;
}

//打开数据
int CSQLiteHelper::OpenDB()
{
	TCHAR	szPath[MAX_PATH] = {0};

	_tcsncpy(szPath, GetAppPth(), MAX_PATH);
	_tcscat(szPath, _T("WBCatch.db"));

	std::string szUTF8 = FormatUTF8(szPath);
	int nRet = sqlite3_open(szUTF8.c_str(), &m_pDB);

	return nRet;
}

//关闭数据库
void CSQLiteHelper::CloseDB()
{
	if (m_pDB)
	{
		sqlite3_close(m_pDB);
	}
}

//执行SQL
int CSQLiteHelper::ExecuteSQL(LPCTSTR szSQL, SQLITECALLPACKPROC Proc, void * pParam)
{
	char* pErrMsg = NULL;
	std::string szUTF8SQL = FormatUTF8(szSQL);
	int nRet = sqlite3_exec(m_pDB, szUTF8SQL.c_str(), Proc, pParam, &pErrMsg);
	if (nRet != SQLITE_OK){
		OutputDebugStringA(pErrMsg);
		sqlite3_free(pErrMsg);
	}
	return nRet;
}
