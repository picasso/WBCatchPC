// PostMessage.h: interface for the CPostMessage class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_POSTMESSAGE_H__A35DDB21_66B2_4F8E_9B28_4D5052F95BBF__INCLUDED_)
#define AFX_POSTMESSAGE_H__A35DDB21_66B2_4F8E_9B28_4D5052F95BBF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CPostMessage  
{
protected:
	CPostMessage();
public:
	virtual ~CPostMessage();
public:
	static CPostMessage * GetObject();
	BOOL RegisterMessage(HWND hWnd, UINT uMsg);
	void PostMessage(UINT uMsg, WPARAM wParam, LPARAM lParam);
	BOOL UnRegisterMessage(UINT uMsg);
private:
	CRITICAL_SECTION				m_cs;
	std::map<UINT,HWND>				m_mapMsg;
};

#endif // !defined(AFX_POSTMESSAGE_H__A35DDB21_66B2_4F8E_9B28_4D5052F95BBF__INCLUDED_)
