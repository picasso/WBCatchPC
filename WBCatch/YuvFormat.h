#ifndef __YUV_FORMAT_H__
#define __YUV_FORMAT_H__

/************************************************************************/
/*函数功能：初始化BMP文件头                                             */
/*参数：dwImageSize图片数据大小(不包含文件头部分的数据)                 */
/************************************************************************/
BITMAPFILEHEADER InitBmpFileHeader(DWORD dwImageSize);

/************************************************************************/
/*函数功能：初始化BMP文件头信息头                                       */
/*参数：dwImageSize图片数据大小(不包含文件头部分的数据)                 */
/*      lWidth图片宽带                                                  */
/*      lHeight图片高度                                                 */
/*      wBitcolor色位(取值1,4,8,24,32)                                  */
/************************************************************************/
BITMAPINFOHEADER InitBmpInfoHeader(long lWidth, long lHeight, DWORD dwImageSize, WORD wBitcolor);

/************************************************************************/
/*函数功能：rgb数据转bmp文件数据                                        */
/*参数：pRGB RGB图片数据                                                */
/*      lWidth图片宽带                                                  */
/*      lHeight图片高度                                                 */
/*      wBitcolor色位(取值1,4,8,24,32)                                  */
/************************************************************************/
BYTE * rgb_bmp(BYTE * pRGB, long lWidth, long lHeight, DWORD dwImageSize, WORD wBitcolor);

/************************************************************************/
/*函数功能：从RGB图片内存中转换HBITMAP句柄                              */
/*参数：pRGB图片内存                                                    */
/*      dwBufferSize图片内存大小                                        */
/*      lWidth图片宽带                                                  */
/*      lHeight图片高度                                                 */
/*      wBitcolor色位(取值1,4,8,24,32)                                  */
/************************************************************************/
HBITMAP CreateHandleFromRGBBuffer(BYTE * pRGB, DWORD dwBufferSize, long lWidth, long lHeight, WORD wBitcolor);

//yuv转rgb
void yuv2rgb(unsigned char *rgbout,unsigned char *yuvin, int width,int height);

#endif