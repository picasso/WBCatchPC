// WBCatch.h : main header file for the WBCATCH application
//

#if !defined(AFX_WBCATCH_H__AE80133C_60C8_4C5D_8C10_8CB1DF5F02ED__INCLUDED_)
#define AFX_WBCATCH_H__AE80133C_60C8_4C5D_8C10_8CB1DF5F02ED__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CWBCatchApp:
// See WBCatch.cpp for the implementation of this class
//

class CWBCatchApp : public CWinApp
{
public:
	CWBCatchApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CWBCatchApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CWBCatchApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
protected:
	Gdiplus::GdiplusStartupInput m_gdiplusStartupInput;
	ULONG_PTR m_pGdiToken;
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_WBCATCH_H__AE80133C_60C8_4C5D_8C10_8CB1DF5F02ED__INCLUDED_)
