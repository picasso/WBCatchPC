#ifndef __SQLITE_H__
#define __SQLITE_H__

typedef int (* SQLITECALLPACKPROC)(void*,int,char**,char**);

class CSQLiteHelper  
{
protected:
	CSQLiteHelper();
public:
	~CSQLiteHelper();
public:
	//获取单体对象
	static CSQLiteHelper * GetObject();
	//执行SQL
	int ExecuteSQL(LPCTSTR szSQL, SQLITECALLPACKPROC Proc, void * pParam);
protected:
	//打开数据
	int OpenDB();
	//关闭数据库
	void CloseDB();
public:
	sqlite3  * m_pDB;
};

#endif
